#ifndef QXIMAGEFEATURE_H
#define QXIMAGEFEATURE_H

#include <QList>
#include <QMatrix>
#include <QTransform>


#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/video/background_segm.hpp>

#include "QxImageClasses.h"

class QxImageFeaturePrivate;

#define DEFAULT_DIFFERANCE 0.1
    enum
    {
        crError  = 0,
        crGreater,
        crLess,
        crEquial
    }typedef ifComparisonResult;

#define FeatureTagCount 3
    enum
    {
        iftImage   = 0,
        iftScale   = 1,
        iftCluster = 2
    } typedef featureTag;

class QxImageFeature
{
public:
    QxImageFeature();
    QxImageFeature(QxImageFeature *feature);
    virtual ~QxImageFeature();
public:
    static float commonThreshold;
    static void  setCommonThreshold(float threshold);
    static float getCommonThreshold();
public:
    void reset();
public:
    bool isValid();
    bool isPoint();

    bool isGlobal();
    void setIsGlobal(bool val);

    void setTag(int tag,featureTag tagId = iftImage);
    int  getTag(featureTag tagId = iftImage);

    void setPoint(float x, float y);
    void setX(float x);
    void setY(float y);
    float x();
    float y();

    void  setSize(float size);
    float getSize();

    void  setAngle(float angle);
    float getAngle();

    void  setResponse(float response);
    float getResponse();

    int  getDescriptorHash();

    void  setOctave(int octave);
    int   getOctave();

    void   setDescriptor(float *descriptor);
    float *getDescriptor();
    float *getDescriptorCopy();
    bool   putDescriptor(float *descriptor);

    void setDescriptorSize(int size);
    int  getDescriptorSize();

    void setRealSizeX(float x);
    float getRealSizeX();

    void setRealSizeY(float y);
    float getRealSizeY();

    bool setFromKeyPoint(cv::KeyPoint &point);
public:
    float compareWithDescriptor(float *inDescriptor);

    bool  isEquialTo(QxImageFeature *inFeature, float *distance = 0);
    bool  isEquialTo(QxImageFeature *inFeature, float epsilon, float *distance = 0);

    ifComparisonResult  compareTo(QxImageFeature *inFeature, float *distance = 0);
    ifComparisonResult  compareTo(QxImageFeature *inFeature, float epsilon, float *distance = 0);
protected:
    QxImageFeaturePrivate *data;
};


class QxImageFeatureListPrivate;

class QxImageFeatureList
{
public:
    QxImageFeatureList();
    virtual ~QxImageFeatureList();
public:
    int  count();
    void clear();
    void clearAndErase();
public:
    void addFeature(QxImageFeature *feature);
    void addFeature(QxImageFeatureList *features);

    void addCopyFeature(QxImageFeature *feature);
    void addCopyFeature(QxImageFeatureList *features);

    QxImageFeature *getFeature(QxImageFeature *feature);
    QxImageFeature *getFeature(int pos);

    void deleteFeature(QxImageFeature *feature);
    void deleteFeature(int pos);

    void removeFeature(QxImageFeature *feature);
    void removeFeature(int pos);

    QxImageFeature *findClosestTo(QxImageFeature *feature, int *position = 0);
    QxImageFeature *findClosestTo(QxImageFeature *feature, int *position, float epsilon);
    QxImageFeature *findClosestTo(QxImageFeature *feature, float epsilon, int *position = 0);

    bool findNNClosestTo(QxImageFeature *feature, QList<QxImageFeature *> *features, int neighbourCount = 1, bool sort = false);
    bool findNNClosestTo(QxImageFeature *feature, QList<QxImageFeature *> *features, float epsilon, int neighbourCount = 1, bool sort = false);
public:
    void findCommon(QxImageFeatureList *with, QList<QxImageFeature *> *commonSelf, QList<QxImageFeature *> *dismatchesSelf = 0, QList<QxImageFeature *> *commonWith = 0, QList<QxImageFeature *> *dismatchesWith = 0);
    void findCommon(QxImageFeatureList *with, QList<QxImageFeature *> *commonSelf, float epsilon, QList<QxImageFeature *> *dismatchesSelf = 0, QList<QxImageFeature *> *commonWith = 0, QList<QxImageFeature *> *dismatchesWith = 0);

    void matchWithFeatures(QxImageFeatureList *with, QList<QxImageFeature *> *matches, QList<QxImageFeature *> *dismatches = 0);
    void matchWithFeatures(QxImageFeatureList *with, QList<QxImageFeature *> *matches, float epsilon, QList<QxImageFeature *> *dismatches = 0);
public:
    static bool findHomography(QList<QxImageFeature *> *src, QList<QxImageFeature *> *dst, QTransform &transform);
public:
    static void putPointsToVector(QList<QxImageFeature *> *features, std::vector<cv::Point2f> &points);
protected:
    QxImageFeatureListPrivate *data;
};

#endif // QXIMAGEFEATURE_H

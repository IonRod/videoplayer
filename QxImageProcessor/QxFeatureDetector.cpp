#include "QxFeatureDetector.h"

class QxFeatureDetectorPrivate
{
    public:
        FeatureDetectorType type;

        //surf params
        double surfHessianThreshold;
        int    surfOctaves;
        int    surfOctaveLayers;
        //end surf params
    public:
        QxFeatureDetectorPrivate()
        {
            type = fdSURF64;

            surfHessianThreshold = 400.;
            surfOctaves = 3;
            surfOctaveLayers = 4;
        }
        ~QxFeatureDetectorPrivate()
        {
            //cvFindHomography()
        }
};

QxFeatureDetector::QxFeatureDetector()
{
    data = new QxFeatureDetectorPrivate;
}

QxFeatureDetector::~QxFeatureDetector()
{
    if( data )
    {
        delete data;
        data = 0;
    }
}

//************************************************************
//
//  System functions start
//
//************************************************************

void QxFeatureDetector::setDetectorType( FeatureDetectorType type)
{
    if( data )
    {
        switch( type )
        {
            default:
            case fdSURF64:
                data->type = fdSURF64;
            break;

            case fdSURF128:
                data->type = fdSURF128;
            break;

            case fdSIFT:
                data->type = fdSIFT;
            break;
        }

    }
}

FeatureDetectorType QxFeatureDetector::getDetectorType()
{
    FeatureDetectorType result = fdNone;
    if( data )
    {
        result = data->type;
    }
    return result;
}

//------------------------------------------------------------
//  System functions end
//------------------------------------------------------------

//************************************************************
//
//  SURF64 and SURF128 functions start
//
//************************************************************

void  QxFeatureDetector::setHessianThreshold(float threshold)
{
    if( data )
    {
        data->surfHessianThreshold = threshold;
    }
}

float QxFeatureDetector::getHessianThreshold()
{
    float result = -1;
    if ( data )
    {
        result = data->surfHessianThreshold;
    }
    return result;
}

void QxFeatureDetector::setOctaves(int octaves)
{
    if( data )
    {
        data->surfOctaves = octaves;
    }
}

int  QxFeatureDetector::getOctaves()
{
    int result = -1;
    if ( data )
    {
        result = data->surfOctaves;
    }
    return result;
}

void QxFeatureDetector::setOctaveLayers(int layers)
{
    if( data )
    {
        data->surfOctaveLayers = layers;
    }
}

int  QxFeatureDetector::getOctaveLayers()
{
    int result = -1;
    if ( data )
    {
        result = data->surfOctaveLayers;
    }
    return result;
}

//############################################################
//  SURF64 and SURF128 functions end
//############################################################

/*------------------------------------------------------------------------*/

void QxFeatureDetector::genereateFeature(QxImageFeature *feature, cv::KeyPoint &point, QxImageFrame *frame, float *discriptor, int discriptorSize)
{
    feature->setFromKeyPoint(point);

    feature->setRealSizeX(frame->width());
    feature->setRealSizeY(frame->height());

    feature->setDescriptorSize(discriptorSize);
    feature->setDescriptor(discriptor);
}

/*------------------------------------------------------------------------*/

bool QxFeatureDetector::findKeyPoints(cv::Mat &image, std::vector<cv::KeyPoint> &points)
{
    bool result = false;
    if( data )
    {
        switch( data->type )
        {
            case fdSURF64:
                this->findKeyPointsSURF64(image, points);
                result = true;
            break;

            case fdSURF128:
                this->findKeyPointsSURF128(image, points);
                result = true;
            break;

            case fdSIFT:
                this->findKeyPointsSIFT(image, points);
                result = true;
            break;
        }
    }
    return result;
}

bool QxFeatureDetector::findDiscriptors(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors)
{
    bool result = false;
    if( data )
    {
        switch( data->type )
        {
            case fdSURF64:
                this->findDiscriptorsSURF64(image, points, discriptors);
                result = true;
            break;

            case fdSURF128:
                this->findDiscriptorsSURF128(image, points, discriptors);
                result = true;
            break;

            case fdSIFT:
                this->findDiscriptorsSIFT(image,points, discriptors);
                result = true;
            break;
        }
    }
    return result;
}

/*------------------------------------------------------------------------*/

void QxFeatureDetector::findKeyPointsSURF64(cv::Mat &image, std::vector<cv::KeyPoint> &points)
{
    cv::SurfFeatureDetector detector(data->surfHessianThreshold, data->surfOctaves, data->surfOctaveLayers, false);
    detector.detect(image, points);
}

void QxFeatureDetector::findDiscriptorsSURF64(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors)
{
    cv::SurfDescriptorExtractor extractor(data->surfOctaves, data->surfOctaveLayers, false);
    extractor.compute(image, points, discriptors);
}

void QxFeatureDetector::findKeyPointsSURF128(cv::Mat &image, std::vector<cv::KeyPoint> &points)
{
    cv::SurfFeatureDetector detector(data->surfHessianThreshold, data->surfOctaves, data->surfOctaveLayers, false);
    detector.detect(image, points);
}

void QxFeatureDetector::findDiscriptorsSURF128(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors)
{
    cv::SurfDescriptorExtractor extractor(data->surfOctaves, data->surfOctaveLayers, true);
    extractor.compute(image, points, discriptors);
}

void QxFeatureDetector::findKeyPointsSIFT(cv::Mat &image, std::vector<cv::KeyPoint> &points)
{

}

void QxFeatureDetector::findDiscriptorsSIFT(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors)
{

}

//############################################################
//  Recognizer functions end
//############################################################



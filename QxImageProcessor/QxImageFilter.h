#ifndef QXIMAGEFILTER_H
#define QXIMAGEFILTER_H

#include <QImage>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "QxImageFrame.h"
/*--------------------------------------------------------------*/
//Base class for filters

class QxImageFilter
{
public:
    QxImageFilter();
    virtual ~QxImageFilter();
public:
    virtual bool apply(QxImageFrame *in);
    virtual bool applyToImage(QxImageFrame *in, QxImageFrame *out);
    virtual QxImageFrame *applyAndCopy(QxImageFrame *in);
protected:

private:
};

/*--------------------------------------------------------------*/
//RescaleFilter

class QxRescaleFilterPrivate;

class QxRescaleFilter: public QxImageFilter
{
public:
    QxRescaleFilter();
    QxRescaleFilter(float scaleFactor);
    QxRescaleFilter(bool isSaveResolution);
    QxRescaleFilter(float scaleFactor, bool isSaveResolution);
    virtual ~QxRescaleFilter();
public:
    virtual bool apply(QxImageFrame *in);
    virtual bool applyToImage(QxImageFrame *in, QxImageFrame *out);
    virtual QxImageFrame *applyAndCopy(QxImageFrame *in);
public:
    void  setScaleFactor(float value = 0.5);
    float getScaleFactor();

    void setIsSaveResolution(bool value = false);
    bool getIsSaveResolution();
protected:
    QxRescaleFilterPrivate *data;
};

/*--------------------------------------------------------------*/
//Blur filter
/*
class QxBlurFilterPrivate;

class QxBlurFilter: public QxImageFilter
{
public:
    QxBlurFilter();
    virtual ~QxBlurFilter();
public:
    virtual bool apply(QxImageFrame *in);
    virtual bool applyToImage(QxImageFrame *in, QxImageFrame *out);
    virtual QxImageFrame *applyAndCopy(QxImageFrame *in);
public:
    void  setScaleFactor(float value = 0.5);

protected:
    QxBlurFilterPrivate *data;
};
*/
#endif // QXIMAGEFILTER_H

#include "QxImagePainter.h"

class QxImagePainterPrivate
{
    public:
        QxDataBase *base;
    public:
        QxImagePainterPrivate()
        {
            base = 0;
        }
        ~QxImagePainterPrivate()
        {

        }
};

QxImagePainter::QxImagePainter()
{
    data = new QxImagePainterPrivate;
}

QxImagePainter::~QxImagePainter()
{
    delete data;
}

void QxImagePainter::drawCircleKeypoints( QImage &image, QxImageFrame *frame)
{

}

void QxImagePainter::drawCircleKeypoints( QImage &image, QxImageFrame *frame, QxImageFeatureList *features, QList<QColor> &colors )
{

}

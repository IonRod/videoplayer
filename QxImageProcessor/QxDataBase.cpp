#include <QDebug>

#include "QxDataBase.h"

#include "QxImageFrame.h"

#include <stdio.h>

typedef struct {
    int index;
    int P;
    int N;
} TldExportEntry;

class QxDataBasePrivate
{
    public:
        QxFeatureDetector *detector;
        tld::TLD *predator;
    public:
        QxImageFeatureList features;
        QxImageFeatureList candidates;


        float minLevel;
    public:
        float threshold;
        float clustering;
    public:
        QxDataBasePrivate()
        {
            detector = 0;
            predator = 0;

            threshold  = 0.1;
            clustering = 0.1;

            minLevel = 0;
        }
        ~QxDataBasePrivate()
        {
            detector = 0;
        }
};

QxDataBase::QxDataBase()
{
    data = new QxDataBasePrivate;
    data->predator = new tld::TLD();

    {
        data->predator->trackerEnabled  = true;
        data->predator->learningEnabled = true;
        data->predator->detectorEnabled = true;
        data->predator->alternating     = false;


        tld::DetectorCascade *detectorCascade    = data->predator->detectorCascade;

        detectorCascade->varianceFilter->enabled     = false;
        detectorCascade->ensembleClassifier->enabled = true;
        detectorCascade->nnClassifier->enabled       = true;

        // classifier
        detectorCascade->useShift = true;
        detectorCascade->shift    = 0.1;

        detectorCascade->minScale = -10;
        detectorCascade->maxScale =  10;

        detectorCascade->minSize  = 25;

        detectorCascade->numTrees    = 10;
        detectorCascade->numFeatures = 10;

        detectorCascade->nnClassifier->thetaTP = 0.65;
        detectorCascade->nnClassifier->thetaFP = 0.4;

    }
}

QxDataBase::~QxDataBase()
{
    data->predator->release();
    delete data->predator;
    delete data;
    data = 0;
}

//************************************************************
//
//  System functions start
//
//************************************************************

/*
        m_useProportionalShift(true),
        m_varianceFilterEnabled(true),
        m_ensembleClassifierEnabled(true),
        m_nnClassifierEnabled(true),
        m_loadModel(false),
        m_trackerEnabled(true),
        m_selectManually(false),
        m_learningEnabled(true),
        m_showOutput(true),
        m_showNotConfident(true),
        m_showColorImage(false),
        m_showDetections(false),
        m_showForeground(false),
        m_saveOutput(false),
        m_alternating(false),
        m_exportModelAfterRun(false),
        m_trajectory(20),
        m_method(IMACQ_CAM),
        m_startFrame(1),
        m_lastFrame(0),
        m_minScale(-10),
        m_maxScale(10),
        m_numFeatures(10),
        m_numTrees(10),
        m_thetaP(0.65),
        m_thetaN(0.5),
        m_minSize(25),
        m_camNo(0),
        m_fps(24),
        m_seed(0),
        m_threshold(0.7),
        m_proportionalShift(0.1),
        m_modelExportFile("model"),
        m_initialBoundingBox(vector<int>())
*/

/*
    main->tld->trackerEnabled = m_settings.m_trackerEnabled;

    main->threshold = m_settings.m_threshold;
    main->showForeground = m_settings.m_showForeground;
    main->showNotConfident = m_settings.m_showNotConfident;

    main->tld->alternating = m_settings.m_alternating;
    main->tld->learningEnabled = m_settings.m_learningEnabled;

    DetectorCascade* detectorCascade = main->tld->detectorCascade;
    detectorCascade->varianceFilter->enabled = m_settings.m_varianceFilterEnabled;
    detectorCascade->ensembleClassifier->enabled = m_settings.m_ensembleClassifierEnabled;
    detectorCascade->nnClassifier->enabled = m_settings.m_nnClassifierEnabled;

    // classifier
    detectorCascade->useShift = m_settings.m_useProportionalShift;
    detectorCascade->shift = m_settings.m_proportionalShift;
    detectorCascade->minScale = m_settings.m_minScale;
    detectorCascade->maxScale = m_settings.m_maxScale;
    detectorCascade->minSize = m_settings.m_minSize;
    detectorCascade->numTrees = m_settings.m_numTrees;
    detectorCascade->numFeatures = m_settings.m_numFeatures;
    detectorCascade->nnClassifier->thetaTP = m_settings.m_thetaP;
    detectorCascade->nnClassifier->thetaFP = m_settings.m_thetaN;

  */
void QxDataBase::setFeatureDetector(QxFeatureDetector *detector)
{
    if( data )
    {
        data->detector = detector;
    }
}

QxFeatureDetector *QxDataBase::getFeatureDetector()
{
    QxFeatureDetector *result = 0;
    if( data )
    {
        if( data->detector)
        {
            result = data->detector;
        }
    }
    return result;
}

void QxDataBase::clear()
{
    if( data != 0)
    {
        data->candidates.clearAndErase();
        data->features.clearAndErase();
    }
}

void QxDataBase::clearCandidates()
{
    if( data != 0)
    {
        data->candidates.clearAndErase();
    }
}

void QxDataBase::setMinLevel(float val)
{
    if( data )
    {
        data->minLevel = val;
    }
}

float QxDataBase::getMinLevel()
{
    if( data )
    {
        return data->minLevel;
    }
    return 0;
}

void QxDataBase::setIsLearning(bool on)
{
    data->predator->learningEnabled = on;
}

void QxDataBase::setLearningLevel(float lvl)
{
    data->predator->level = lvl;
}

//############################################################
//  System functions end
//############################################################

/*------------------------------------------------------------------------*/

bool QxDataBase::updateDetector(int width, int height, int step)
{
    tld::DetectorCascade *detectorCascade    = data->predator->detectorCascade;

    detectorCascade->imgWidth     = width;
    detectorCascade->imgHeight    = height;
    detectorCascade->imgWidthStep = step;


    detectorCascade->initWindowsAndScales();
    detectorCascade->initWindowOffsets();

    detectorCascade->propagateMembers();

    detectorCascade->initialised = true;

    detectorCascade->ensembleClassifier->initFeatureOffsets();
}

bool QxDataBase::update(QList<QxImageFrame *> &frames)
{
    bool result = false;

    if ( frames.count()>0 )
    {
        QxImageFrame *frame;
        cv::Mat      image;
        cv::Rect     rct;
        int tmp;

        int count = frames.count();

        if( count )
        {
            frame = frames[0];

            tmp  = frame->width();

            if ( frame->width()%4 )
            {
                tmp += 4-frame->width()%4;
            }

            rct.x = 2;
            rct.y = 2;
            rct.width  = frame->width()-4;
            rct.height = frame->height()-4;



            tld::DetectorCascade *detectorCascade    = data->predator->detectorCascade;

            image  =  cv::Mat(frame->height(), frame->width(), CV_8UC1, (uchar *) frame->getImageGrayscaleBits(), tmp);
            int size = frame->width();

            tmp = size;
            if ( size % 4 )
            {
                tmp += 4 - size % 4;
            }

            detectorCascade->imgWidth     = frame->width();
            detectorCascade->imgHeight    = frame->height();
            detectorCascade->imgWidthStep = tmp;
            data->predator->selectObject(image, &rct);

            for( int i =  1; i < count; ++i)
            {
                frame = frames[i];
                image  =  cv::Mat(frame->height(), frame->width(), CV_8UC1, (uchar *) frame->getImageGrayscaleBits(), tmp);
                data->predator->initialLearning(image);
            }
        }
    }
    return result;
}

bool QxDataBase::detect(QxImageFrame *frame, QxImageFrame *prevImage)
{
    bool result = false;

    if( frame )
    {

        if( !frame->getImageGrayscaleBits() )
        {
            return false;
        }

        cv::Mat      image;
        cv::Mat      prevIma;
        QRect        faceRct;
        cv::Rect     rct;
        bool flag = true;
        int tmp;

        tmp  = frame->width();

        if ( frame->width()%4 )
        {
            tmp += 4-frame->width()%4;
        }

        rct.x = 0;
        rct.y = 0;
        rct.width  = frame->width();
        rct.height = frame->height();

        image  =  cv::Mat(frame->height(), frame->width(), CV_8UC1, (uchar *) frame->getImageGrayscaleBits(), tmp);

        if( prevImage )
        {
            if( prevImage->getProb() >= data->minLevel )
            {
                flag = false;
                prevIma = cv::Mat(prevImage->height(), prevImage->width(), CV_8UC1, (uchar *) prevImage->getImageGrayscaleBits(), tmp);
                faceRct = prevImage->getFaceRect();
            }
        }

        if( flag )
        {
            data->predator->processImage(image);
        }
        else
        {
            bool tmp;

            if( prevImage->getProb() >= 0.4 )
            {
                tmp = true;
            }
            else
            {
                tmp = false;
            }
            data->predator->processImage(image, &prevIma, faceRct, tmp);
        }


        if( data->predator->currBB )
        {
            faceRct.setX( data->predator->currBB->x );
            faceRct.setY( data->predator->currBB->y );

            faceRct.setWidth ( data->predator->currBB->width  );
            faceRct.setHeight( data->predator->currBB->height );

            frame->setProb( data->predator->currConf );
            frame->setFaceRect( faceRct );

            if( !frame->getIsSample() )
            {
                frame->setIsSample(data->predator->learning);
            }
            result = true;
        }
        else
        {
            frame->setProb(0);
        }

    }
    return result;
}

bool QxDataBase::save(QString path)
{
    bool result = false;
    tld::NNClassifier * nn = data->predator->detectorCascade->nnClassifier;
    tld::EnsembleClassifier* ec = data->predator->detectorCascade->ensembleClassifier;

    FILE * file = fopen(path.toStdString().c_str(), "w");
    fprintf(file,"#Tld ModelExport\n");
    fprintf(file,"%d #width\n", data->predator->detectorCascade->objWidth);
    fprintf(file,"%d #height\n", data->predator->detectorCascade->objHeight);
    fprintf(file,"%f #min_var\n", data->predator->detectorCascade->varianceFilter->minVar);
    fprintf(file,"%d #Positive Sample Size\n", nn->truePositives->size());



    for(size_t s = 0; s < nn->truePositives->size();s++) {
        float * imageData =nn->truePositives->at(s).values;
        for(int i = 0; i < TLD_PATCH_SIZE; i++) {
            for(int j = 0; j < TLD_PATCH_SIZE; j++) {
                fprintf(file, "%f ", imageData[i*TLD_PATCH_SIZE+j]);
            }
            fprintf(file, "\n");
        }
    }

    fprintf(file,"%d #Negative Sample Size\n", nn->falsePositives->size());

    for(size_t s = 0; s < nn->falsePositives->size();s++) {
        float * imageData = nn->falsePositives->at(s).values;
        for(int i = 0; i < TLD_PATCH_SIZE; i++) {
            for(int j = 0; j < TLD_PATCH_SIZE; j++) {
                fprintf(file, "%f ", imageData[i*TLD_PATCH_SIZE+j]);
            }
            fprintf(file, "\n");
        }
    }

    fprintf(file,"%d #numtrees\n", ec->numTrees);
    data->predator->detectorCascade->numTrees = ec->numTrees;
    fprintf(file,"%d #numFeatures\n", ec->numFeatures);
    data->predator->detectorCascade->numFeatures = ec->numFeatures;
    for(int i = 0; i < ec->numTrees; i++) {
        fprintf(file, "#Tree %d\n", i);

        for(int j = 0; j < ec->numFeatures; j++) {
            float * features = ec->features + 4*ec->numFeatures*i + 4*j;
            fprintf(file,"%f %f %f %f # Feature %d\n", features[0], features[1], features[2], features[3], j);
        }

        //Collect indices
        vector<TldExportEntry> list;

        for(int index = 0; index < pow(2.0f, ec->numFeatures); index++) {
            int p = ec->positives[i * ec->numIndices + index];
            if(p != 0) {
                TldExportEntry entry;
                entry.index = index;
                entry.P = p;
                entry.N = ec->negatives[i * ec->numIndices + index];
                list.push_back(entry);
            }
        }

        fprintf(file,"%d #numLeaves\n", list.size());
        for(size_t j = 0; j < list.size(); j++) {
            TldExportEntry entry = list.at(j);
            fprintf(file,"%d %d %d\n", entry.index, entry.P, entry.N);
        }
    }

    fclose(file);
    return result;
}

bool QxDataBase::load(QString path)
{
    bool result = false;

    data->predator->release();

    tld::NNClassifier * nn = data->predator->detectorCascade->nnClassifier;
    tld::EnsembleClassifier* ec = data->predator->detectorCascade->ensembleClassifier;

    FILE * file = fopen(path.toStdString().c_str(), "r");

    if(file == NULL)
    {
        printf("Error: Model not found: %s\n", path);
        return false;
    }

    int MAX_LEN=255;
    char str_buf[255];
    fgets(str_buf, MAX_LEN, file); /*Skip line*/

    fscanf(file,"%d \n", &data->predator->detectorCascade->objWidth);
    fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/
    fscanf(file,"%d \n", &data->predator->detectorCascade->objHeight);
    fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/

    fscanf(file,"%f \n", &data->predator->detectorCascade->varianceFilter->minVar);
    fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/

    int numPositivePatches;
    fscanf(file, "%d \n", &numPositivePatches);
    fgets(str_buf, MAX_LEN, file); /*Skip line*/


    for(int s = 0; s < numPositivePatches; s++)
    {
        tld::NormalizedPatch patch;

        for(int i = 0; i < 15; i++) { //Do 15 times

            fgets(str_buf, MAX_LEN, file); /*Read sample*/

            char * pch;
            pch = strtok (str_buf," ");
            int j = 0;
            while (pch != NULL)
            {
                float val = atof(pch);
                patch.values[i*TLD_PATCH_SIZE+j] = val;

                pch = strtok (NULL, " ");

                j++;
            }
        }

        nn->truePositives->push_back(patch);
    }

    int numNegativePatches;
    fscanf(file, "%d \n", &numNegativePatches);
    fgets(str_buf, MAX_LEN, file); /*Skip line*/


    for(int s = 0; s < numNegativePatches; s++) {
        tld::NormalizedPatch patch;
        for(int i = 0; i < 15; i++) { //Do 15 times

            fgets(str_buf, MAX_LEN, file); /*Read sample*/

            char * pch;
            pch = strtok (str_buf," ");
            int j = 0;
            while (pch != NULL)
            {
                float val = atof(pch);
                patch.values[i*TLD_PATCH_SIZE+j] = val;

                pch = strtok (NULL, " ");

                j++;
            }
        }

        nn->falsePositives->push_back(patch);
    }

    fscanf(file,"%d \n", &ec->numTrees);
    data->predator->detectorCascade->numTrees = ec->numTrees;
    fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/

    fscanf(file,"%d \n", &ec->numFeatures);
    data->predator->detectorCascade->numFeatures = ec->numFeatures;
    fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/

    int size = 2 * 2 * ec->numFeatures * ec->numTrees;
    ec->features = new float[size];
    ec->numIndices = pow(2.0f, ec->numFeatures);
    ec->initPosteriors();

    for(int i = 0; i < ec->numTrees; i++) {
        fgets(str_buf, MAX_LEN, file); /*Skip line*/

        for(int j = 0; j < ec->numFeatures; j++) {
            float * features = ec->features + 4*ec->numFeatures*i + 4*j;
            fscanf(file, "%f %f %f %f",&features[0], &features[1], &features[2], &features[3]);
            fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/
        }

        /* read number of leaves*/
        int numLeaves;
        fscanf(file,"%d \n", &numLeaves);
        fgets(str_buf, MAX_LEN, file); /*Skip rest of line*/

        for(int j = 0; j < numLeaves; j++) {
            TldExportEntry entry;
            fscanf(file,"%d %d %d \n", &entry.index, &entry.P, &entry.N);
            ec->updatePosterior(i, entry.index, 1, entry.P);
            ec->updatePosterior(i, entry.index, 0, entry.N);
        }
    }

    data->predator->detectorCascade->initWindowsAndScales();
    data->predator->detectorCascade->initWindowOffsets();

    data->predator->detectorCascade->propagateMembers();

    data->predator->detectorCascade->initialised = true;

    ec->initFeatureOffsets();

    return result;
}

/*------------------------------------------------------------------------*/

QxImageFeatureList *QxDataBase::buildImageFeatures(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;
    if( (data != 0) && (frame != 0))
    {
        if( data->detector )
        {
            cv::Mat originalDiscriptors;

            int tmp  = frame->width();
            if ( frame->width()%4 )
            {
                tmp += 4-frame->width()%4;
            }

            cv::Mat originalImage(frame->height(), frame->width(), CV_8UC1, (uchar *)frame->getImageGrayscaleBits(), tmp);

            std::vector<cv::KeyPoint> originalPoints;

            data->detector->findKeyPoints(originalImage, originalPoints);
            data->detector->findDiscriptors(originalImage, originalPoints, originalDiscriptors);

            this->createFeatureList(frame, originalImage, originalPoints, originalDiscriptors);


            result = frame->getImageFeatureList();
        }
    }
    return result;
}

QxImageFeatureList *QxDataBase::proceedImage(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;
    if( (data != 0) && (frame != 0))
    {
        if( data->detector )
        {
            QxImageFeatureList *features;
            QxImageFeature     *feature;

            cv::Mat tempDiscriptors;
            cv::Mat originalDiscriptors;

            cv::Mat tempImage;

            int tmp  = frame->width();
            if ( frame->width()%4 )
            {
                tmp += 4-frame->width()%4;
            }

            cv::Mat originalImage(frame->height(), frame->width(), CV_8UC1, (uchar *)frame->getImageGrayscaleBits(), tmp);

            tempImage = originalImage.clone();

            std::vector<cv::KeyPoint> tempPoints;
            std::vector<cv::KeyPoint> originalPoints;

            cv::KeyPoint point;

            float *discriptor;
            int    discriptorSize;
            int    originalPointsCount;

            features = frame->getImageFeatureList();

/*------------------------------------------------------------------------*/
            data->detector->findKeyPoints(originalImage, originalPoints);
            data->detector->findDiscriptors(originalImage, originalPoints, originalDiscriptors);

            originalPointsCount = originalPoints.size();

            if( originalPointsCount > 0 )
            {

            }
/*------------------------------------------------------------------------*/
            if( originalPointsCount > 0 )
            {
                this->createFeatureList(frame, originalImage, originalPoints, originalDiscriptors);
            }
            result = frame->getImageFeatureList();
        }
    }
    return result;
}

QxImageFeatureList *QxDataBase::proceedImageAndAdd(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;
    if( data != 0)
    {
        result = this->proceedImage(frame);
        if( result )
        {
            this->addCandidate( result );
        }
    }
    return result;
}

bool QxDataBase::addCandidate(QxImageFeature *feature)
{
    bool result = false;
    if( (data != 0) && (feature != 0))
    {
        QxImageFeature *candidate = new QxImageFeature(feature);
        this->data->candidates.addFeature(candidate);
        result = true;
    }
    return result;
}

bool QxDataBase::addCandidate(QxImageFeatureList *features)
{
    bool result = false;
    if( (data != 0) && (features != 0))
    {
        this->data->candidates.addCopyFeature( features );
        result = true;
    }
    return result;
}

bool QxDataBase::addFeatureToBase(QxImageFeature *feature)
{
    bool result = false;
    if( (data != 0) && (feature != 0))
    {
        if( feature->isValid() )
        {
            QxImageFeature *global = new QxImageFeature(feature);
            this->data->features.addFeature(global);
            global->setIsGlobal(true);
            result = true;
        }
    }
    return result;
}

bool QxDataBase::addFeatureToBase(QxImageFeatureList *features)//TODO: create
{
    bool result = false;
    if( (data != 0) && (features != 0))
    {

    }
    return result;
}

bool QxDataBase::proceedCandidates()
{
    bool result = false;
    if( data != 0)
    {
        QList<QxImageFeatureList *>clusters;
        QxImageFeatureList *cluster;

        QList<QxImageFeature *>clusterCenters;

        QxImageFeature *candidate;
        QxImageFeature *clusterCenter;

        int clusterCount;
        int count;
        int descriptorSize;
        bool added = false;

        float differance;

        float *clusterDescriptor;
        float *candidateDescriptor;

        float clusterWeight;
        float candidateWeight;
        float weight;

        count = data->candidates.count();
        clusterCount = 0;

        for( int i=0; i<count; ++i)
        {
            added = false;

            candidate = data->candidates.getFeature(i);

            for( int j=0; j<clusterCount; ++j)
            {

                clusterCenter = clusterCenters.at(j);

                if( candidate->isEquialTo(clusterCenter, data->clustering, &differance))
                {
                    cluster = clusters.at(j);
                    cluster->addFeature(candidate);

                    clusterDescriptor   = clusterCenter->getDescriptor();
                    candidateDescriptor =     candidate->getDescriptor();

                    clusterWeight   = clusterCenter->getResponse();
                    candidateWeight = candidate->getResponse();

                    weight = clusterWeight + candidateWeight;
                    //printf("Weight cluster[%i]: %10f Weight candidate: %10f Responce: %10f\n", j, clusterWeight, candidateWeight, clusterWeight*candidateWeight/weight*2.);
                    clusterCenter->setResponse( clusterWeight*candidateWeight/weight*2. );
                    //clusterCenter->setResponse( (clusterWeight*clusterWeight+candidateWeight*candidateWeight)/weight );

                    clusterWeight   /= weight;
                    candidateWeight /= weight;

                    descriptorSize = clusterCenter->getDescriptorSize();
                    for( int k=0; k< descriptorSize; k++)
                    {
                        clusterDescriptor[k] = clusterWeight*clusterDescriptor[k] + candidateWeight*candidateDescriptor[k];
                    }

                    added = true;
                    break;
                }
            }

            if( !added )
            {
                clusterCount ++;

                cluster = new QxImageFeatureList;
                cluster->addFeature(candidate);
                clusters.append(cluster);

                clusterCenter = new QxImageFeature(candidate);
                clusterCenters.append(clusterCenter);
            }
        }

        /*printf("Candidate count: %i\n", count);
        printf("Cluster   count: %i\n", clusterCount);
*/
        for( int i=0; i< clusterCount; ++i)
        {
            clusterCenter = clusterCenters.at(i);
           // printf("    Cluster[%3i] weight: %f\n", i, clusterCenter->getResponse());
            data->features.addFeature( clusterCenter );
        }

        data->candidates.clearAndErase();

        result = true;
    }
    return result;
}

//############################################################
//  Image processing functions end
//############################################################

bool QxDataBase::checkFeature(QxImageFeature *point, int *index, float *responce)
{
    bool result = false;
    if( (data) && (point) )
    {
        if( point->isValid())
        {
            QxImageFeature *feature;
            feature = data->features.findClosestTo(point, index, data->threshold);
            if( feature )
            {
                result = true;
                if( responce )
                {
                    *responce = feature->getResponse();
                }
            }
        }
    }
    return result;
}

void QxDataBase::setTreshhold(float val)
{
    if( data )
    {
        data->threshold = val;
    }
}

void QxDataBase::setClustering(float val)
{
    if( data )
    {
        data->clustering = val;
    }
}

/*------------------------------------------------------------------------*/

void QxDataBase::createFeatureList(QxImageFrame *frame, cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors)
{
    if( (frame) && (data) )
    {
        //if( data->detector )
        {

            QxImageFeature *feature;
            QxImageFeatureList *lst;

            int    count;
            int    discriptorSize;

            count = points.size();
            lst   = frame->getImageFeatureList();
            discriptorSize = discriptors.cols;

            for( int i = 0; i<count; ++i)
            {
                feature = new QxImageFeature();
                if( feature )
                {
                    feature->setFromKeyPoint(points.at(i));

                    feature->setRealSizeX(frame->width());
                    feature->setRealSizeY(frame->height());

                    feature->setDescriptorSize(discriptorSize);
                    feature->setDescriptor((float *)(discriptors.ptr(i)));

                    lst->addFeature(feature);

                    feature = 0;
                }
            }
        }
    }
}

/*------------------------------------------------------------------------*/

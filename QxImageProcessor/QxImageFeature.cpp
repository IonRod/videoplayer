#include "QxImageFeature.h"

#include <QDebug>

class QxImageFeaturePrivate
{
    public:
        bool isValid;
        bool isGlobal;
        int tag[FeatureTagCount];

        float x;
        float y;

        float realX;
        float realY;

        float size;
        float angle;
        float response;
        int   octave;

        int   descriptorSize;
        float *descriptor;
    public:
        QxImageFeaturePrivate()
        {
            isValid  = false;
            isGlobal = false;

            for( int i=0; i<FeatureTagCount; ++i)
            {
                tag[i] = -1;
            }

            x = -1;
            y = -1;

            realX = -1;
            realY = -1;

            size = -1;
            angle = -1;
            response = -1;
            octave = -1;

            descriptorSize = 64;
            descriptor = 0;
        }
        ~QxImageFeaturePrivate()
        {
            descriptor = 0;
        }
};

QxImageFeature::QxImageFeature()
{
    data = new QxImageFeaturePrivate;
}

QxImageFeature::QxImageFeature(QxImageFeature *feature)
{
    data = new QxImageFeaturePrivate;
    if( feature )
    {
        data->isValid  = feature->data->isValid;
        data->isGlobal = feature->data->isGlobal;

        for( int i=0; i< FeatureTagCount; ++i)
        {
            data->tag[i]=feature->data->tag[i];
        }

        data->x = feature->data->x;
        data->y = feature->data->y;

        data->realX = feature->data->realX;
        data->realY = feature->data->realY;

        data->angle    = feature->data->angle;
        data->size     = feature->data->size;
        data->response = feature->data->response;
        data->octave   = feature->data->octave;

        data->descriptor     = feature->getDescriptorCopy();
        data->descriptorSize = feature->data->descriptorSize;
    }
}

QxImageFeature::~QxImageFeature()
{
    if( data != 0)
    {
        if( data->descriptor != 0)
        {
            delete []data->descriptor;
            data->descriptor = 0;
        }
        delete data;
        data = 0;
    }
}

/*------------------------------------------------------------------------*/

float QxImageFeature::commonThreshold = DEFAULT_DIFFERANCE;

void  QxImageFeature::setCommonThreshold(float threshold)
{
    if( threshold > 0)
    {
        commonThreshold = threshold;
    }
}

float QxImageFeature::getCommonThreshold()
{
    return commonThreshold;
}

/*------------------------------------------------------------------------*/

void QxImageFeature::reset()
{
    if( data )
    {
        data->isValid  = false;
        data->isGlobal = false;

        for( int i=0; i<FeatureTagCount; ++i)
        {
            data->tag[i] = -1;
        }

        data->x = -1;
        data->y = -1;

        data->size     = -1;
        data->angle    = -1;
        data->response = -1;
        data->octave   = -1;

        if( data->descriptor )
        {
            delete []data->descriptor;
            data->descriptor = 0;
        }
    }
}

bool QxImageFeature::isValid()
{
    bool result = false;
    if( data )
    {
        result = true;

        if( data->x < 0 )
        {
            result = false;
        }

        if( data->y < 0 )
        {
            result = false;
        }

        if( data->size < 0 )
        {
            result = false;
        }

        if( data->angle < 0 )
        {
            result = false;
        }

        if( data->octave < 0 )
        {
            result = false;
        }

        if( data->response < 0 )
        {
            result = false;
        }

        if( data->descriptor == 0 )
        {
            result = false;
        }
    }
    return result;
}

bool QxImageFeature::isPoint()
{
    bool result = false;
    if( data )
    {
        result = true;

        if( data->x < 0 )
        {
            result = false;
        }

        if( data->y < 0 )
        {
            result = false;
        }

    }
    return result;
}

bool QxImageFeature::isGlobal()
{
    bool result = false;
    if( data )
    {
        result = data->isGlobal;
    }
    return result;
}

void QxImageFeature::setIsGlobal(bool val)
{
    if( data )
    {
        data->isGlobal = val;
    }
}

void QxImageFeature::setTag(int tag, featureTag tagId)
{
    if( data )
    {
        if( (tagId >= 0) && (tagId<FeatureTagCount))
        {
            data->tag[tagId] = tag;
        }
    }
}

int  QxImageFeature::getTag(featureTag tagId)
{
    int result = -1;
    if( data )
    {
        if( (tagId >= 0) && (tagId<FeatureTagCount))
        {
            result = data->tag[tagId];
        }
    }
    return result;
}

///
void QxImageFeature::setPoint(float x, float y)
{
    if( data )
    {
        data->x = x;
        data->y = y;
        data->isValid = true;
    }
}

void QxImageFeature::setX(float x)
{
    if( data )
    {
        data->x = x;
    }
}

void QxImageFeature::setY(float y)
{
    if( data )
    {
        data->y = y;
    }
}

float QxImageFeature::x()
{
    float result = -1;
    if( data )
    {
        result = data->x;
    }
    return result;
}

float QxImageFeature::y()
{
    float result = -1;
    if( data )
    {
        result = data->y;
    }
    return result;
}

void  QxImageFeature::setSize(float size)
{
    if( data )
    {
        data->size = size;
    }
}

float QxImageFeature::getSize()
{

    float result = -1;
    if( data )
    {
        result = data->size;
    }
    return result;
}

void  QxImageFeature::setAngle(float angle)
{
    if( data )
    {
        data->angle = angle;
    }
}

float QxImageFeature::getAngle()
{
    float result = -1;
    if( data )
    {
        result = data->angle;
    }
    return result;
}

void  QxImageFeature::setResponse(float response)
{
    if( data )
    {
        data->response = response;
    }
}

float QxImageFeature::getResponse()
{
    float result = -1;
    if( data )
    {
        result = data->response;
    }
    return result;
}

int  QxImageFeature::getDescriptorHash()
{
    int result = -1;
    if( data )
    {
        if( data->descriptor )
        {
            float value = 0;
            for( int i=0; i<data->descriptorSize; i++)
            {
                value += fabs(data->descriptor[i]);
            }

            result = abs( value*1000 );
        }
    }
    return result;
}

void  QxImageFeature::setOctave(int octave)
{
    if( data )
    {
        data->octave = octave;
    }
}

int   QxImageFeature::getOctave()
{
    int result = -1;
    if( data )
    {
        result = data->octave;
    }
    return result;
}

void   QxImageFeature::setDescriptor(float *descriptor)
{
    if( (data) && (descriptor) )
    {
        if( data->descriptor == 0)
        {
            data->descriptor = new float[data->descriptorSize];
        }
        for( int i=0; i<data->descriptorSize; ++i)
        {
            data->descriptor[i] = descriptor[i];
        }
    }
}

float *QxImageFeature::getDescriptor()
{
    float *result = 0;
    if( data )
    {
        if( data->descriptor != 0)
        {
            result = data->descriptor;
        }
    }
    return result;
}

float *QxImageFeature::getDescriptorCopy()
{
    float *result = 0;
    if( data )
    {
        if( data->descriptor != 0)
        {
            result = new float[data->descriptorSize];
            for( int i=0; i<data->descriptorSize; i++)
            {
                result[i]=data->descriptor[i];
            }
        }

    }
    return result;
}

bool   QxImageFeature::putDescriptor(float *descriptor)
{
    bool result = false;
    if( (data) && (descriptor) )
    {
        if( data->descriptor != 0)
        {
            for( int i=0; i<data->descriptorSize; i++)
            {
                descriptor[i]=data->descriptor[i];
            }
            result = true;
        }

    }
    return result;
}

void QxImageFeature::setDescriptorSize(int size)
{
    if( data )
    {
        if( size > 0)
        {
            data->descriptorSize = size;
        }
    }
}

int  QxImageFeature::getDescriptorSize()
{
    int result = -1;
    if( data )
    {
       result = data->descriptorSize;
    }
    return result;
}

void QxImageFeature::setRealSizeX(float x)
{
    if( data )
    {
       data->realX = x;
    }
}

float QxImageFeature::getRealSizeX()
{
    float result = -1;
    if( data )
    {
       result = data->realX;
    }
    return result;
}

void QxImageFeature::setRealSizeY(float y)
{
    if( data )
    {
       data->realY = y;
    }
}

float QxImageFeature::getRealSizeY()
{
    float result = -1;
    if( data )
    {
       result = data->realX;
    }
    return result;
}

bool QxImageFeature::setFromKeyPoint(cv::KeyPoint &point)
{
    bool result = false;
    if( data )
    {
        data->x = point.pt.x;
        data->y = point.pt.y;

        data->size     = point.size;
        data->angle    = point.angle;
        data->octave   = point.octave;
        data->response = point.response;

    }
    return result;
}

/*------------------------------------------------------------------------*/

float QxImageFeature::compareWithDescriptor(float *inDescriptor)
{
    float result = -1;
    if( (data) && (inDescriptor != 0) )
    {
        if( data->descriptor != 0)
        {
            double     tmp = 0;
            double lengtIn = 0;
            double length  = 0;

            for( int i = 0; i< data->descriptorSize; i++)
            {
                tmp += (inDescriptor[i]*data->descriptor[i]);
                lengtIn += inDescriptor[i]*inDescriptor[i];
                length  += data->descriptor[i]*data->descriptor[i];

                //tmp += (inDescriptor[i]-data->descriptor[i])*(inDescriptor[i]-data->descriptor[i]);
            }

            tmp = fabs(tmp);
            lengtIn = sqrt(lengtIn);
            length = sqrt(length);

            if( (lengtIn*length<0.0000001) )
            {
                if( tmp < 0.0000001)
                {
                    tmp = 0;
                }
            }
            else
            {
                tmp /= lengtIn;
                tmp /= length;
            }
            result = 1.0-tmp;
        }
    }
    return result;
}

bool QxImageFeature::isEquialTo(QxImageFeature *inFeature, float *distance)
{
    return this->isEquialTo(inFeature, commonThreshold, distance);
}

bool QxImageFeature::isEquialTo(QxImageFeature *inFeature, float epsilon, float *distance)
{
    bool result = false;
        ifComparisonResult responce = this->compareTo(inFeature, epsilon, distance);
        if( responce != crError )
        {
            result = true;
        }
    return result;
}

ifComparisonResult QxImageFeature::compareTo(QxImageFeature *inFeature, float *distance)//-1 in > self, 0 - == 1 self > in
{
    return this->compareTo(inFeature, commonThreshold, distance);
}

ifComparisonResult QxImageFeature::compareTo(QxImageFeature *inFeature, float epsilon, float *distance)//-1 in > self, 0 - == 1 self > in
{
    ifComparisonResult result = crError;
    if( (data) && (inFeature != 0) )
    {
        if( data->descriptorSize == inFeature->data->descriptorSize )
        {
            float differance = compareWithDescriptor(inFeature->getDescriptor()) ;

            if( (differance < epsilon) && (differance>=0.0) )
            {
                if( data->response < inFeature->getResponse() )
                {
                    result = crLess;
                }
                else
                {
                    if( data->response > inFeature->getResponse() )
                    {
                        result = crGreater;
                    }
                    else
                    {
                        result = crEquial;
                    }
                }
                if( distance )
                {
                    *distance = differance;
                }
            }
        }
    }
    return result;
}
//Class

class QxImageFeatureListPrivate
{
    public:
        QList<QxImageFeature *>features;
    public:
        QxImageFeatureListPrivate()
        {

        }
        ~QxImageFeatureListPrivate()
        {

        }
};

QxImageFeatureList::QxImageFeatureList()
{
    data = new QxImageFeatureListPrivate;
}

QxImageFeatureList::~QxImageFeatureList()
{
    if( data )
    {
        this->clearAndErase();
        delete data;
        data = 0;
    }
}

/*------------------------------------------------------------------------*/

int QxImageFeatureList::count()
{
    int result = 0;
    if( data != 0)
    {
        result = data->features.count();
    }
    return result;
}

void QxImageFeatureList::clear()
{
    if( data )
    {
        data->features.clear();
    }
}

void QxImageFeatureList::clearAndErase()
{
    if( data )
    {
        QxImageFeature *feature;
        int count;
        count = data->features.count();
        for( int i=0; i<count; ++i)
        {
            feature = data->features.at(i);
            delete feature;
        }
        data->features.clear();
    }
}

void QxImageFeatureList::addFeature(QxImageFeature *feature)
{
    if( (data) &&(feature) )
    {
        data->features.append(feature);
    }
}

void QxImageFeatureList::addFeature(QxImageFeatureList *features)
{
    if( (data != 0) &&( features != 0) )
    {
        int count = 0;
        QxImageFeature *feature = 0;
        count = features->count();
        for( int i=0; i<count; i++)
        {
            feature = features->getFeature(i);
            if( feature )
            {
                data->features.append(feature);
            }
        }
    }
}

void QxImageFeatureList::addCopyFeature(QxImageFeature *feature)
{
    if( (data != 0) &&( feature != 0) )
    {
        QxImageFeature *temp = new QxImageFeature(feature);
        if(temp)
        {
            data->features.append(temp);
        }
    }
}

void QxImageFeatureList::addCopyFeature(QxImageFeatureList *features)
{
    if( (data != 0) &&( features != 0) )
    {
        int count = 0;
        QxImageFeature *feature = 0;
        QxImageFeature *temp = 0;
        count = features->count();
        for( int i=0; i<count; i++)
        {
            feature = features->getFeature(i);
            if( feature )
            {
                temp = new QxImageFeature( feature);
                if( temp )
                {
                    data->features.append(temp);
                }
            }
        }
    }
}

QxImageFeature *QxImageFeatureList::getFeature(QxImageFeature *feature)
{
    QxImageFeature *result = 0;
    if( (data != 0) && ( feature != 0) )
    {
        int index = data->features.indexOf(feature);
        if( index >=0)
        {
            result = data->features.at(index);
        }
    }
    return result;
}

QxImageFeature *QxImageFeatureList::getFeature(int pos)
{
    QxImageFeature *result = 0;
    if( data != 0 )
    {
        if( (pos >=0) && (pos<data->features.count()))
        {
            result = data->features.at(pos);
        }
    }
    return result;
}

void QxImageFeatureList::deleteFeature(QxImageFeature *feature)
{
    if( data != 0)
    {
        int index;

        index = data->features.indexOf(feature);
        if( index >=0 )
        {
            data->features.removeAt(index);
            delete feature;
        }
    }
}

void QxImageFeatureList::deleteFeature(int pos)
{
    if( data != 0)
    {
        if( ( pos>=0 ) && (pos<data->features.count()))
        {
            QxImageFeature *temp;
            temp = data->features.at(pos);
            if( temp )
            {
                delete temp;
            }
            data->features.removeAt(pos);
        }
    }
}

void QxImageFeatureList::removeFeature(QxImageFeature *feature)
{
    if( data != 0)
    {
        int index;

        index = data->features.indexOf(feature);
        if( index >=0 )
        {
            data->features.removeAt(index);
        }
    }
}

void QxImageFeatureList::removeFeature(int pos)
{
    if( data != 0)
    {
        if( ( pos>=0 ) && (pos<data->features.count()))
        {
            data->features.removeAt(pos);
        }
    }
}

QxImageFeature *QxImageFeatureList::findClosestTo(QxImageFeature *feature, int *position)
{
    return this->findClosestTo(feature, QxImageFeature::getCommonThreshold(), position);
}

QxImageFeature *QxImageFeatureList::findClosestTo(QxImageFeature *feature, int *position, float epsilon)
{
    return this->findClosestTo(feature, epsilon, position);
}

QxImageFeature *QxImageFeatureList::findClosestTo(QxImageFeature *feature, float epsilon, int *position)
{
    QxImageFeature *result = 0;

    if( (data) && (feature) )
    {
        if( feature->isValid() )
        {
            QxImageFeature *temp;
            int count = data->features.count();

            int   pos = -1;
            float minDistanse;
            float distanse;

            for( int i=0; i<count; ++i )
            {
                temp = data->features.at(i);
                if( temp )
                {
                    if( feature->isEquialTo(temp, epsilon, &distanse) )
                    {
                        minDistanse = distanse;
                        pos         = i;
                        break;
                    }
                }
            }

            if( pos != -1)
            {
                for( int i=pos+1; i<count; ++i )
                {
                    temp = data->features.at(i);
                    if( temp )
                    {
                        if( feature->isEquialTo(temp, epsilon, &distanse) )
                        {
                            if( distanse<minDistanse )
                            {
                                minDistanse = distanse;
                                pos         = i;
                            }
                        }
                    }
                }

                if( position )
                {
                    *position = pos;
                }
                result = data->features.at(pos);
            }
        }
    }
    return result;
}

bool QxImageFeatureList::findNNClosestTo(QxImageFeature *feature, QList<QxImageFeature *> *features, int neighbourCount, bool sort)
{
    return this->findNNClosestTo( feature, features, QxImageFeature::getCommonThreshold(), neighbourCount, sort);
}

bool QxImageFeatureList::findNNClosestTo(QxImageFeature *feature, QList<QxImageFeature *> *features, float epsilon, int neighbourCount, bool sort)
{
    bool result = false;

    if( (data) && (feature) && (features) && (neighbourCount>0) )
    {
        if( feature->isValid() )
        {
            QList<float> minDifferanse;
            QList<QxImageFeature *> positions;

            QxImageFeature *temp;
            float distanse;

            int count = data->features.count();

            int i, j;

            for(i=0; i<count; ++i)
            {
                temp = data->features.at(i);
                if( temp )
                {
                    if( feature->isEquialTo(temp, epsilon, &distanse) )
                    {
                        j = 0;
                        for(; j<minDifferanse.count(); ++j)
                        {
                            if( distanse < minDifferanse.at(j) )
                            {
                                break;
                            }
                        }

                        minDifferanse.insert(j, distanse);
                        positions.insert(j, temp);

                        if(minDifferanse.count() >= neighbourCount)
                        {
                            break;
                        }
                    }
                }
            }

            if( minDifferanse.count() )
            {
                for(i++; i<count; ++i)
                {
                    temp = data->features.at(i);
                    if( temp )
                    {
                        if( feature->isEquialTo(temp, epsilon, &distanse) )
                        {
                            j = 0;
                            for(; j<minDifferanse.count(); ++j)
                            {
                                if( distanse < minDifferanse.at(j) )
                                {
                                    minDifferanse.insert(j, distanse);
                                    positions.insert(j, temp);

                                    if(minDifferanse.count() >= neighbourCount)
                                    {
                                        minDifferanse.removeLast();
                                        positions.removeLast();
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                features->append(positions);
                result = true;
            }
        }
    }

    return result;
}

/*----------------------------------------------------------*/

void QxImageFeatureList::findCommon(QxImageFeatureList *with, QList<QxImageFeature *> *commonSelf, QList<QxImageFeature *> *dismatchesSelf, QList<QxImageFeature *> *commonWith, QList<QxImageFeature *> *dismatchesWith)
{
    this->findCommon(with, commonSelf, QxImageFeature::getCommonThreshold(), dismatchesSelf, commonWith, dismatchesWith);
}

void QxImageFeatureList::findCommon(QxImageFeatureList *with, QList<QxImageFeature *> *commonSelf, float epsilon, QList<QxImageFeature *> *dismatchesSelf, QList<QxImageFeature *> *commonWith, QList<QxImageFeature *> *dismatchesWith)
{
    if( (data) && (with) && (commonSelf))
    {
        int selfCount = 0;
        int withCount = 0;

        bool isCommon = false;
        QList<QxImageFeature *> &selfFeatures = data->features;
        QList<QxImageFeature *> &withFeatures = with->data->features;

        QxImageFeature *selfFeature = 0;
        QxImageFeature *withFeature = 0;
        QxImageFeature *tempFeature = 0;

        selfCount = selfFeatures.count();
        withCount = withFeatures.count();

        for( int i = 0; i < selfCount; i++)
        {
            isCommon = false;
            selfFeature = selfFeatures.at(i);
            if( !selfFeature )
                continue;

            withFeature = with->findClosestTo(selfFeature, epsilon);
            tempFeature = this->findClosestTo(withFeature, epsilon);

            if( selfFeature->isEquialTo(tempFeature, 0.0001))
            {
                isCommon   = true;
                commonSelf->append(selfFeature);
                if( commonWith )
                    commonWith->append(withFeature);
            }

            if( !isCommon )
            {
                if((dismatchesSelf) && (selfFeature))
                {
                    dismatchesSelf->append(selfFeature);
                }
            }
        }

        if( (dismatchesWith) && (commonWith) )
        {
            bool isIn;
            selfCount = commonWith->count();

            for( int i = 0; i<withCount; ++i)
            {
                isIn = false;
                withFeature = withFeatures.at(i);

                for(int j = 0; j<selfCount; ++j)
                {
                    if( withFeature->isEquialTo(commonWith->at(j), 0.0001) )
                    {
                        isIn = true;
                        break;
                    }
                }

                if( !isIn )
                {
                    dismatchesWith->append(withFeature);
                }
            }
        }
    }
}

void QxImageFeatureList::matchWithFeatures(QxImageFeatureList *with, QList<QxImageFeature *> *matches, QList<QxImageFeature *> *dismatches)
{
    this->matchWithFeatures(with, matches, QxImageFeature::getCommonThreshold(), dismatches);
}

void QxImageFeatureList::matchWithFeatures(QxImageFeatureList *with, QList<QxImageFeature *> *matches, float epsilon, QList<QxImageFeature *> *dismatches)
{
    if( (data) && (with) && (matches) )
    {
        int selfCount = 0;
        int withCount = 0;

        QList<QxImageFeature *> &selfFeatures = data->features;
        QList<QxImageFeature *> &withFeatures = with->data->features;

        QxImageFeature *selfFeature = 0;
        QxImageFeature *withFeature = 0;

        selfCount = selfFeatures.count();
        withCount = withFeatures.count();

        bool equial = false;
        int selfPos  = 0;
        int withPos  = 0;

        for( selfPos = 0; selfPos < selfCount; selfPos++)
        {
            equial = false;
            selfFeature = selfFeatures.at(selfPos);

            if( selfFeature )
            {
                if( selfFeature->isValid() )
                {
                    for( withPos = 0; withPos < withCount; withPos++)
                    {
                        withFeature = withFeatures.at(withPos);
                        if( selfFeature->isEquialTo(withFeature, epsilon))
                        {
                            equial = true;
                            break;
                        }
                    }
                }

                if( equial )
                {
                    matches->append(selfFeature);
                }
                else
                {
                    if( dismatches )
                    {
                        dismatches->append(selfFeature);
                    }
                }
            }
        }
    }
}


//************************************************************
//
//  Global static functions start
//
//************************************************************

bool QxImageFeatureList::findHomography(QList<QxImageFeature *> *src, QList<QxImageFeature *> *dst, QTransform &transform)
{
    bool result = false;
    if( (src) && (dst) )
    {
        if( (src->count() == dst->count()) && (src->count() >= 4))
        {
            std::vector<cv::Point2f> srcPoints;
            std::vector<cv::Point2f> dstPoints;

            cv::Mat projection;

            putPointsToVector(src, srcPoints);
            putPointsToVector(dst, dstPoints);

            projection = cv::findHomography(srcPoints, dstPoints, cv::LMEDS, 10);
            //projection = cv::findFundamentalMat(srcPoints, dstPoints);

            transform.reset();

            qDebug()<<projection.at<double>(0,0)<<" "<<projection.at<double>(1,0)<<" "<<projection.at<double>(2,0);
            qDebug()<<projection.at<double>(0,1)<<" "<<projection.at<double>(1,1)<<" "<<projection.at<double>(2,1);
            qDebug()<<projection.at<double>(0,2)<<" "<<projection.at<double>(1,2)<<" "<<projection.at<double>(2,2)<<endl;

            transform.setMatrix( projection.at<double>(0,0), projection.at<double>(1,0), projection.at<double>(2,0),
                                 projection.at<double>(0,1), projection.at<double>(1,1), projection.at<double>(2,1),
                                 projection.at<double>(0,2), projection.at<double>(1,2), projection.at<double>(2,2)
                                 );

            /*transform.setMatrix( projection.at<double>(0,0), projection.at<double>(1,0), 0,
                                 projection.at<double>(0,1), projection.at<double>(1,1), 0,
                                 1, 1, projection.at<double>(2,2)
                                 );*/


            if( fabs(transform.determinant()) < 100. )
            {
                result = true;
            }
            //matrix.setMatrix(projection.at<double>(0,0), projection.at<double>(0,1), projection.at<double>(1,0), projection.at<double>(1,1), projection.at<double>(0,2), projection.at<double>(1,2));
            //QMatrix(projection.at<float>(0, 0), projection.at<float>(0, 1), projection.at<float>(1, 0), projection.at<float>(1, 1), projection.at<float>(2, 0), projection.at<float>(2, 1));

        }
    }
    return result;
}

/*------------------------------------------------------------------------*/

void QxImageFeatureList::putPointsToVector(QList<QxImageFeature *> *features, std::vector<cv::Point2f> &points)
{
    QxImageFeature *feature;
    for( int i=0; i<features->count(); ++i )
    {
        feature = features->at(i);
        //point = new cv::Point2f(feature->x(), feature->y());
        //points.push_back(cv::Point2f(feature->x()-feature->getRealSizeX()*0.5, feature->y()-feature->getRealSizeY()*0.5));
        points.push_back(cv::Point2f(feature->x(), feature->y()));
    }
}

//------------------------------------------------------------
//  Global functions end
//------------------------------------------------------------

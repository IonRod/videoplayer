#ifndef QXIMAGEMANAGER_H
#define QXIMAGEMANAGER_H

#include <QList>

#include "QxImageFrame.h"

class QxImageManagerPrivate;
class QxImageFrame;

class QxImageManager
{
public:
    QxImageManager(bool isVideo = false);
    virtual ~QxImageManager();
public:
    void addImage( QxImageFrame *frame);
    void incInMemoryCount();
    void val(int position);

    QxImageFrame *getImage( int framePosition);
    QxImageFrame *getImageByFrame( int frameNumber);
    QxImageFrame *getImageByTime( float frameTime);
public:
    void deleteImage( int framePosition);
    void deleteImageByFrame( int frameNumber);
    void deleteImageByTime( float frameTime);
public:
    int  count();
    void clear();
    void sort();
    void setIsVideo( bool val);
    bool getIsVideo();
protected:
    QxImageManagerPrivate *data;
};

#endif // QXIMAGEMANAGER_H

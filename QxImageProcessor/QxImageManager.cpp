#include "QxImageManager.h"

class QxImageManagerPrivate
{
    public:
        QList<QxImageFrame *> list;

        int maxFrameNumber;
        int minFrameNumber;

        float maxFrameTime;
        float minFrameTime;

        int storeInMemory;
        int inMemory;

        bool isVideo;
    public:
        QxImageManagerPrivate()
        {
            maxFrameNumber = 0;
            minFrameNumber = 0;

            maxFrameTime = -1;
            minFrameTime = -1;

            storeInMemory = 150;
            inMemory = 0;

            isVideo = false;
        }
        ~QxImageManagerPrivate()
        {

        }
};

QxImageManager::QxImageManager( bool isVideo)
{
    data = new QxImageManagerPrivate;
    if( data != 0)
    {
        data->isVideo = isVideo;
    }
}

QxImageManager::~QxImageManager()
{
    if( data != 0)
    {
        this->clear();
        delete data;
    }
}

//############################################################
//
//  Image managment functions
//
//############################################################
void QxImageManager::addImage(QxImageFrame *frame)
{
    if( (data) )
    {
        int position = data->list.count();
        if( data->isVideo )
        {
            int   num = frame->getFrameNumber();
            float time = frame->getFrameTime();

            int count = data->list.count();

            if( 0 )
            {

            }
            else
            {
                if( data->maxFrameTime < 0 )
                {
                    data->minFrameTime = time;
                    data->maxFrameTime = time;
                }

                if( time <= data->minFrameTime )
                {
                    position = 0;
                    data->minFrameNumber = num;
                    data->minFrameTime = time;
                }
                else
                {
                    if( time > data->maxFrameTime)
                    {
                        position = count;
                        data->maxFrameNumber = num;
                        data->maxFrameTime = time;
                    }
                    else
                    {
                        QxImageFrame *temp;

                        for( position = 0; position<count; position++)
                        {
                            temp = data->list.at(position);
                            if( temp != 0)
                            {
                                if( temp->getFrameTime() >= 0)
                                {
                                    if( temp->getFrameTime() > time)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        data->list.insert(position, frame);
        frame->setFramePosition(position);
        data->inMemory++;
        if( data->inMemory > data->storeInMemory )
        {
            int i, end;
            QxImageFrame *temp;

            end = data->list.count();

            for( i = 0; i<end; i++  )
            {
                if( (i < position-data->storeInMemory/2) || (i > position+data->storeInMemory/2))
                {
                    temp = data->list[i];
                    if( temp != 0)
                    {
                        if( temp->getImageRGBBits() )
                        {
                            temp->unloadFromMemory();
                            data->inMemory--;
                        }
                    }
                }

                if( data->inMemory < data->storeInMemory/2 )
                {
                    break;
                }
            }
        }
    }
}

void QxImageManager::incInMemoryCount()
{
    data->inMemory++;
}

void QxImageManager::val(int position)
{
    if( data->inMemory > data->storeInMemory )
    {
        int i, end;
        QxImageFrame *temp;

        end = data->list.count();

        for( i = 0; i<end; i++  )
        {
            if( (i < position-data->storeInMemory/2) || (i > position+data->storeInMemory/2))
            {
                temp = data->list[i];
                if( temp != 0)
                {
                    if( temp->getImageRGBBits() )
                    {
                        temp->unloadFromMemory();
                        data->inMemory--;
                    }
                }
            }

            if( data->inMemory < data->storeInMemory/2 )
            {
                break;
            }
        }
    }
}

QxImageFrame *QxImageManager::getImage(int framePosition)
{
    QxImageFrame *result = 0;
    if( data != 0)
    {
        if( (framePosition >=0) && (framePosition < data->list.count()))
        {
            result = data->list.at(framePosition);
            result->setFramePosition(framePosition);
        }
    }
    return result;
}

QxImageFrame *QxImageManager::getImageByFrame(int frameNumber)
{
    QxImageFrame *result = 0;
    if( (data != 0) &&(frameNumber >=0))
    {
        int count = data->list.count();
        QxImageFrame *temp;
        for( int i = 0; i<count; ++i)
        {
            temp = data->list.at(i);
            if( temp->getFrameNumber() != -1)
            {
                if( temp->getFrameNumber() == frameNumber)
                {
                    result = temp;
                    result->setFramePosition(i);
                    break;
                }
            }
        }
    }
    return result;
}

QxImageFrame *QxImageManager::getImageByTime(float frameTime)
{
    QxImageFrame *result = 0;
    if( (data != 0) )
    {
        if( ( frameTime+20 >= this->data->minFrameTime) && (frameTime-20 < this->data->maxFrameTime))
        {
            QxImageFrame *temp;
            int start  = 0;
            int finish = data->list.count()-1;
            int delta;
            int pos, prevPos;

            prevPos = start+finish+10;

            //std::cout<<"Search: "<<frameTime<<std::endl;

            while( start <= finish )
            {

                pos = (start+finish) / 2;
                if( prevPos == pos )
                {
                    break;
                }
                prevPos = pos;
                temp = data->list[pos];

                delta = temp->getFrameTime() - frameTime;

                //std::cout<<"\tStart: "<<start<<" Finish: "<<finish<<std::endl;
                //std::cout<<"\tPos: "<<pos<<" Delta: "<<delta<<std::endl;

                if( (delta < 20) && (delta >= -20) )
                {
                    result = temp;
                    result->setFramePosition(pos);
                    break;
                }
                else
                {
                    if( delta > 0 )
                    {
                        finish = pos;
                    }
                    else
                    {
                        start = pos+1;
                    }
                }
            }

        }
    }
    return result;
}

void QxImageManager::deleteImage( int framePosition)
{

}

void QxImageManager::deleteImageByFrame( int frameNumber)
{

}

void QxImageManager::deleteImageByTime( float frameTime)
{

}

//************************************************************
//  Image managment functions end
//************************************************************


//############################################################
//
//  System functions start
//
//############################################################

int  QxImageManager::count()
{
    int result = 0;
    if( data != 0)
    {
        result = data->list.count();
    }
    return result;
}

void QxImageManager::clear()
{
    if( data != 0)
    {
        int count = data->list.count();
        QxImageFrame *frame;
        for( int i=0; i< count; ++i )
        {
            frame = data->list.at(i);
            delete frame;
        }
        data->list.clear();

        data->maxFrameNumber = 0;
        data->minFrameNumber = 0;

        data->maxFrameTime = -1;
        data->minFrameTime = -1;

        data->inMemory = 0;
    }
}

void QxImageManager::sort()
{
    if( data != 0)
    {

    }
}

void QxImageManager::setIsVideo( bool val)
{
    if( data )
    {
        data->isVideo = val;
    }
}

bool QxImageManager::getIsVideo()
{
    bool result = false;
    if( data )
    {
        result = data->isVideo;
    }
    return result;
}

//************************************************************
//  System functions end
//************************************************************

#ifndef QXFEATUREDETECTOR_H
#define QXFEATUREDETECTOR_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/video/background_segm.hpp>

#include "QxImageClasses.h"
#include "QxImageFrame.h"
#include "QxImageFeature.h"


enum
{
    fdNone,
    fdSURF64,
    fdSURF128,
    fdSIFT
}typedef FeatureDetectorType;

class QxFeatureDetectorPrivate;

class QxFeatureDetector
{
public:
    QxFeatureDetector();
    ~QxFeatureDetector();
public:
    void setDetectorType( FeatureDetectorType type);
    FeatureDetectorType getDetectorType();
public://surf surf128
    void  setHessianThreshold(float threshold);
    float getHessianThreshold();
    void setOctaves(int octaves);
    int  getOctaves();
    void setOctaveLayers(int layers);
    int  getOctaveLayers();
private:
    void genereateFeature(QxImageFeature *feature, cv::KeyPoint &point, QxImageFrame *frame, float *discriptor, int discriptorSize);
public:
    bool findKeyPoints(cv::Mat &image, std::vector<cv::KeyPoint> &points);
    bool findDiscriptors(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors);
private:

    void findKeyPointsSURF64  (cv::Mat &image, std::vector<cv::KeyPoint> &points);
    void findDiscriptorsSURF64(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors);

    void findKeyPointsSURF128  (cv::Mat &image, std::vector<cv::KeyPoint> &points);
    void findDiscriptorsSURF128(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors);

    void findKeyPointsSIFT   (cv::Mat &image, std::vector<cv::KeyPoint> &points);
    void findDiscriptorsSIFT (cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors);

    void detectFeatureSURF64 (cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptor);
    void detectFeatureSURF128(cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptor);
    void detectFeatureSIFT   (cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptor);

    QxImageFeatureList *proceedImageSURF64(QxImageFrame *frame);
    QxImageFeatureList *proceedImageSURF128(QxImageFrame *frame);
    QxImageFeatureList *proceedImageSIFT(QxImageFrame *frame);
protected:
    QxFeatureDetectorPrivate *data;
};

#endif // QXFEATUREDETECTOR_H

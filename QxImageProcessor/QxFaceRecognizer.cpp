#include "QxFaceRecognizer.h"

class QxFaceRecognizerPrivate
{
    public:
        QxImageManager *pictures;
        QxImageManager *video;
    public:
        //QVideoDecoder *decoder;
    public:
        QxFaceRecognizerPrivate()
        {
            pictures = 0;
            video = 0;

            //decoder = 0;
        }

        ~QxFaceRecognizerPrivate()
        {
            pictures = 0;
            video = 0;

            //decoder = 0;
        }
};

QxFaceRecognizer::QxFaceRecognizer()
{
    data = new QxFaceRecognizerPrivate;
}

QxFaceRecognizer::~QxFaceRecognizer()
{
    if( data != 0)
    {
        if( data->pictures != 0)
        {
            delete data->pictures;
            data->pictures = 0;
        }

        if( data->video != 0)
        {
            delete data->video;
            data->video = 0;
        }
        delete data;
        data = 0;
    }
}

//************************************************************
//
//  System functions start
//
//************************************************************

bool QxFaceRecognizer::loadData(QString path)
{
    bool result = false;
    return result;
}


//############################################################
//  System functions end
//############################################################

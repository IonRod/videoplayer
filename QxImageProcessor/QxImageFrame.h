#ifndef QXIMAGEFRAME_H
#define QXIMAGEFRAME_H


#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <memory.h>

#include "QxImageClasses.h"
#include "QxImageManager.h"
#include "QxImageFeature.h"

class QxImageFramePrivate;

class QxImageFrame
{
public:
    enum
    {
        pfRGB,
        pfBGR,
        pfRGBA,
        pfBGRA,
        pfARGB,
        pfABGR
    } typedef ifPixelFormat;

public:
    QxImageFrame();
    virtual ~QxImageFrame();

public:
    void copyTo(QxImageFrame *dst);
    void copyHeader(QxImageFrame *dst);
    bool imageFromData(int width, int height, unsigned char *bits, int bytesPerLine, ifPixelFormat format = pfRGB);
    //bool imageFromData( int width, int height, unsigned char *bits, int start, int bpp=3, int step = 3, bool invert = false);
    void createGrayscale();
    //void createGrayscaleFrom(unsigned char *src);
    unsigned char *getImageRGBBits();
    void           putImageRGBInto(unsigned char *bits, int step = 3, int bpp = 3, int start = 0);
    unsigned char *getImageGrayscaleBits();
    void           putImageGrayscaleInto(unsigned char *bits, int step = 3, int bpp = 3, int start = 0);
    void clear();
    void reset();
    void unloadFromMemory();
    void resetFeatures();
public:
    void                setImageFeatureList(QxImageFeatureList *features);
    QxImageFeatureList *getImageFeatureList();
public:
    int width();
    int height();
    int bytesPerLine();
    bool isInit();
    bool isValid();
public:
    void  setFrameTime(float time);
    float getFrameTime();
    void  setFrameNumber(int frame);
    int   getFrameNumber();
    void  setFramePosition(int val);
    int   getFramePosition();
//
public:
    void  setProb(float prob);
    float getProb();
    void  setFaceRect(QRect &rct);
    QRect getFaceRect();

    void setFaceImage(unsigned char *img);
    unsigned char *getFaceImage();
    void clearFaceImage();

    void setIsSample(bool on);
    bool getIsSample();
protected:
    QxImageFramePrivate *data;
};

#endif // QXIMAGEFRAME_H

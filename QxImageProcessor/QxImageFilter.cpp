#include "QxImageFilter.h"


//************************************************************
//
//  QxImageFilter functions start
//
//************************************************************


QxImageFilter::QxImageFilter()
{

}

QxImageFilter::~QxImageFilter()
{

}


bool QxImageFilter::apply(QxImageFrame *in)
{
    bool result = false;

    return result;
}

bool QxImageFilter::applyToImage(QxImageFrame *in, QxImageFrame *out)
{
    bool result = false;

    return result;
}

QxImageFrame *QxImageFilter::applyAndCopy(QxImageFrame *in)
{
    QxImageFrame *result = 0;

    return result;
}


//------------------------------------------------------------
//  QxImageFilter functions end
//------------------------------------------------------------


//************************************************************
//
//  RescaleFilter functions start
//
//************************************************************
class QxRescaleFilterPrivate
{
    public:
        QImage tmp;

    public:
        float scaleFactor;
    public:
        bool isSaveResolution;
        bool isValid;
    public:
        QxRescaleFilterPrivate()
        {
            scaleFactor = 1.0;

            isSaveResolution = false;
            isValid          = true;
        }

        ~QxRescaleFilterPrivate()
        {

        }
};


QxRescaleFilter::QxRescaleFilter()
    :QxImageFilter()
{
    data = new QxRescaleFilterPrivate;
}

QxRescaleFilter::QxRescaleFilter(float scaleFactor)
    :QxImageFilter()
{
    data = new QxRescaleFilterPrivate;
    if( data )
    {
        if( scaleFactor>0 )
        {
            data->scaleFactor = scaleFactor;
        }
    }
}

QxRescaleFilter::QxRescaleFilter(bool isSaveResolution)
    :QxImageFilter()
{
    data = new QxRescaleFilterPrivate;
    if( data )
    {
        data->isSaveResolution = isSaveResolution;
    }
}

QxRescaleFilter::QxRescaleFilter(float scaleFactor, bool isSaveResolution)
    :QxImageFilter()
{
    data = new QxRescaleFilterPrivate;
    if( data )
    {
        if( scaleFactor>0 )
        {
            data->scaleFactor = scaleFactor;
        }
        else
        {
            data->isValid = false;
        }

        data->isSaveResolution = isSaveResolution;
    }
}

QxRescaleFilter::~QxRescaleFilter()
{
    if( data )
    {
        delete data;
        data = 0;
    }
}

/*------------------------------------------------------------------------*/

bool QxRescaleFilter::apply(QxImageFrame *in)
{
    bool result = false;
    if( (in) && (data))
    {

    }
    return result;
}

bool QxRescaleFilter::applyToImage(QxImageFrame *in, QxImageFrame *out)
{
    bool result = false;
    if( (in) && (data) && (out) )
    {

    }
    return result;
}

QxImageFrame *QxRescaleFilter::applyAndCopy(QxImageFrame *in)
{
    QxImageFrame *result = 0;

    if( (in) && (data))
    {
        if( (in->isValid()) && (data->isValid) )
        {
            QSize newSize(in->width(), in->height());
            newSize *= data->scaleFactor;

            if( newSize.isValid() )
            {
                QxImageFrame *frame = new QxImageFrame();
                QImage image(in->getImageRGBBits(), in->width(), in->height(), QImage::Format_RGB888);

                image = image.scaled(newSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

                if(data->isSaveResolution )
                {
                    newSize.setWidth(in->width());
                    newSize.setHeight(in->height());
                    image = image.scaled(newSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
                }

                if(image.format() != QImage::Format_RGB888 )
                {
                    image = image.convertToFormat(QImage::Format_RGB888);
                }

                if(frame->imageFromData(newSize.width(), newSize.height(), image.bits(), image.bytesPerLine()) )
                {
                    result = frame;
                }
                else
                {
                    delete frame;
                }
            }
        }
    }

    return result;
}

/*------------------------------------------------------------------------*/
void  QxRescaleFilter::setScaleFactor(float value)
{
    if( data )
    {
        if( value>0 )
        {
            data->scaleFactor = value;
            data->isValid = true;
        }
        else
        {
            data->isValid = false;
        }
    }
}

float QxRescaleFilter::getScaleFactor()
{
    float result = -1;
    if( data )
    {
        result = data->scaleFactor;
    }
    return result;
}

void QxRescaleFilter::setIsSaveResolution(bool value)
{
    if( data )
    {
        data->isSaveResolution = value;
    }
}

bool QxRescaleFilter::getIsSaveResolution()
{
    bool result = false;
    if( data )
    {
        result = data->isSaveResolution;
    }
    return result;
}

//------------------------------------------------------------
//  nil functions end
//------------------------------------------------------------

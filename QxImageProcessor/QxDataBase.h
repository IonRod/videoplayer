#ifndef QXDATABASE_H
#define QXDATABASE_H

#include <QDir>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/video/background_segm.hpp>

#include "QxImageClasses.h"
#include "QxImageFeature.h"
#include "QxFeatureDetector.h"


#include "tld/TLD.h"

class QxDataBasePrivate;

class QxDataBase
{
public:
    QxDataBase();
    ~QxDataBase();
public:
    void setFeatureDetector(QxFeatureDetector *detector);
    QxFeatureDetector *getFeatureDetector();

    void clear();
    void clearCandidates();
public:
    void  setMinLevel(float val);
    float getMinLevel();

    void setIsLearning(bool on);
    void setLearningLevel(float lvl);
public:
    bool updateDetector(int width, int height, int step);
    bool update(QList<QxImageFrame *> &frames);
    bool detect(QxImageFrame *frame, QxImageFrame *prevImage = 0);
    //bool detect(QxImageFrame *frame, QList<QRect> &rect, QList<float> &probability, QxImageFrame *prevImage = 0);
public:
    bool save(QString path);
    bool load(QString path);
public:
    QxImageFeatureList *buildImageFeatures(QxImageFrame *frame);
    QxImageFeatureList *proceedImage(QxImageFrame *frame);
    QxImageFeatureList *proceedImageAndAdd(QxImageFrame *frame);
    bool addCandidate(QxImageFeature *feature);
    bool addCandidate(QxImageFeatureList *features);
    bool addFeatureToBase(QxImageFeature *feature);
    bool addFeatureToBase(QxImageFeatureList *features);

    bool proceedCandidates();
public:
    bool checkFeature(QxImageFeature *point, int *index = 0, float *responce = 0);

    void setTreshhold(float val);
    void setClustering(float val);
private:
    void createFeatureList(QxImageFrame *frame, cv::Mat &image, std::vector<cv::KeyPoint> &points, cv::Mat &discriptors);
protected:
    QxDataBasePrivate *data;
};

#endif // QXDATABASE_H

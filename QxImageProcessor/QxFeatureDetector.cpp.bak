#include "QxFeatureDetector.h"

class QxFeatureDetectorPrivate
{
    public:
        FeatureDetectorType type;

        //surf params
        double surfHessianThreshold;
        int    surfOctaves;
        int    surfOctaveLayers;
        //end surf params
    public:
        QxFeatureDetectorPrivate()
        {
            type = fdSURF64;

            surfHessianThreshold = 400.;
            surfOctaves = 3;
            surfOctaveLayers = 4;
        }
        ~QxFeatureDetectorPrivate()
        {

        }
};

QxFeatureDetector::QxFeatureDetector()
{
    data = new QxFeatureDetectorPrivate;
}

QxFeatureDetector::~QxFeatureDetector()
{
    if( data )
    {
        delete data;
        data = 0;
    }
}

//************************************************************
//
//  System functions start
//
//************************************************************

void QxFeatureDetector::setDetectorType( FeatureDetectorType type)
{
    if( data )
    {
        switch( type )
        {
            default:
            case fdSURF64:
                data->type = fdSURF64;
            break;

            case fdSURF128:
                data->type = fdSURF128;
            break;

            case fdSIFT:
                data->type = fdSIFT;
            break;
        }

    }
}

FeatureDetectorType QxFeatureDetector::getDetectorType()
{
    FeatureDetectorType result = fdNone;
    if( data )
    {
        result = data->type;
    }
    return result;
}

//############################################################
//  System functions end
//############################################################

//************************************************************
//
//  SURF64 and SURF128 functions start
//
//************************************************************

void  QxFeatureDetector::setHessianThreshold(float threshold)
{
    if( data )
    {
        data->surfHessianThreshold = threshold;
    }
}

float QxFeatureDetector::getHessianThreshold()
{
    float result = -1;
    if ( data )
    {
        result = data->surfHessianThreshold;
    }
    return result;
}

void QxFeatureDetector::setOctaves(int octaves)
{
    if( data )
    {
        data->surfOctaves = octaves;
    }
}

int  QxFeatureDetector::getOctaves()
{
    int result = -1;
    if ( data )
    {
        result = data->surfOctaves;
    }
    return result;
}

void QxFeatureDetector::setOctaveLayers(int layers)
{
    if( data )
    {
        data->surfOctaveLayers = layers;
    }
}

int  QxFeatureDetector::getOctaveLayers()
{
    int result = -1;
    if ( data )
    {
        result = data->surfOctaveLayers;
    }
    return result;
}

//############################################################
//  SURF64 and SURF128 functions end
//############################################################


//************************************************************
//
//  Recognizer functions start
//
//************************************************************

QxImageFeatureList *QxFeatureDetector::proceedImage(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;

    if( (data != 0) && (frame != 0))
    {
        switch( data->type )
        {
            default:
            case fdSURF64:
                result = proceedImageSURF64(frame);
            break;

            case fdSURF128:
                result = proceedImageSURF128(frame);
            break;

            case fdSIFT:
                result = proceedImageSIFT(frame);
            break;
        }
    }
    return result;
}

bool QxFeatureDetector::proceedImageInto(QxImageFrame *frame, QxImageFeatureList *features)
{
    bool result = false;
    return result;
}


QxImageFeatureList *QxFeatureDetector::proceedImageSURF64(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;
    {
        QxImageFeatureList *features;
        features = frame->getImageFeatureList();
        unsigned char *bits = 0;
        if( !features )
        {
            features = new QxImageFeatureList;
            frame->setImageFeatureList( features );
        }
        else
        {
            features->clear();
        }
        result = features;

        bits = frame->getImageGrayscaleBits();
        if( bits )
        {
            QxImageFeature *feature = 0;
            cv::Mat matr(frame->height(), frame->width(), CV_8UC1, (uchar *)bits);
            cv::Mat descriptors;
            float *descriptor;
            cv::SurfFeatureDetector     detector( data->surfHessianThreshold, data->surfOctaves, data->surfOctaveLayers, false);
            cv::SurfDescriptorExtractor extractor( data->surfOctaves, data->surfOctaveLayers, false);

            std::vector<cv::KeyPoint> points;
            cv::KeyPoint *point;
            int count = 0;

            detector.detect(matr, points);
            extractor.compute( matr, points, descriptors);
            count = points.size();

            for( int i = 0; i< count; ++i)
            {
                point = &points.at(i);
                feature = new QxImageFeature();
                if( feature )
                {
                    feature->setPoint(point->pt.x, point->pt.y);
                    feature->setSize(point->size);
                    feature->setAngle(point->angle);
                    feature->setOctave(point->octave);
                    feature->setResponse(point->response);
                    feature->setDescriptorSize(descriptors.cols);
                    descriptor =(float *)(descriptors.ptr(i));

                    feature->setDescriptor(descriptor);
                    features->addFeature(feature);
                    feature = 0;
                }
            }
        }
    }
    return result;
}

QxImageFeatureList *QxFeatureDetector::proceedImageSURF128(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;
    //return 0;
    {
        QxImageFeatureList *features;
        features = frame->getImageFeatureList();
        unsigned char *bits = 0;
        if( !features )
        {
            features = new QxImageFeatureList;
            frame->setImageFeatureList( features );
        }
        else
        {
            features->clear();
        }
        result = features;

        bits = frame->getImageGrayscaleBits();
        if( bits )
        {
            QxImageFeature *feature = 0;
            cv::Mat matr(frame->height(), frame->width(), CV_8UC1, (uchar *)bits);
            cv::Mat descriptors;
            float *descriptor;
            cv::SurfFeatureDetector detector( data->surfHessianThreshold, data->surfOctaves, data->surfOctaveLayers, false);
            cv::SurfDescriptorExtractor extractor( data->surfOctaves, data->surfOctaveLayers, true);

            std::vector<cv::KeyPoint> points;
            cv::KeyPoint *point;
            int count = 0;

            detector.detect(matr, points);
            extractor.compute( matr, points, descriptors);
            count = points.size();

            for( int i = 0; i< count; ++i)
            {
                point = &points.at(i);
                feature = new QxImageFeature();
                if( feature )
                {
                    feature->setPoint(point->pt.x, point->pt.y);
                    feature->setSize(point->size);
                    feature->setAngle(point->angle);
                    feature->setOctave(point->octave);
                    feature->setResponse(point->response);
                    feature->setDescriptorSize(descriptors.cols);
                    descriptor =(float *)(descriptors.ptr(i));

                    feature->setDescriptor(descriptor);
                    features->addFeature(feature);
                    feature = 0;
                }
            }
        }
    }
    return result;
}

QxImageFeatureList *QxFeatureDetector::proceedImageSIFT(QxImageFrame *frame)
{
    QxImageFeatureList *result = 0;

    return result;
}

//############################################################
//  Recognizer functions end
//############################################################



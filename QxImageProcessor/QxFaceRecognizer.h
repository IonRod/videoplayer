#ifndef QXFACERECOGNIZER_H
#define QXFACERECOGNIZER_H

#include <Qt>
#include <QString>

#include "../Global.h"
#include "QxImageClasses.h"
#include "QxImageFrame.h"

#ifndef OS_MAC_OS
    #include "QVideoDecoder.h"
#endif

class QxFaceRecognizerPrivate;

class QxFaceRecognizer
{
public:
    QxFaceRecognizer();
    ~QxFaceRecognizer();

public:
    bool loadData( QString path);
#ifndef OS_MAC_OS
        bool setVideoDecoder( QVideoDecoder *decoder );
#endif
protected:
    QxFaceRecognizerPrivate *data;
};

#endif // QXFACERECOGNIZER_H

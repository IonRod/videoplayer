#include "QxImageFrame.h"

class QxImageFramePrivate
{
    public:
        int width;
        int height;
        int bytesPerLine;

        int   frameNumber;
        float frameTime;

        unsigned char *imageRGB;
        unsigned char *imageGreyscale;

        int position;

        bool isInit;

        QxImageFeatureList *features;

        float prob;

        bool isSample;
    public:
        QRect faceRect;
        unsigned char *faceImage;
    public:
        QxImageFramePrivate()
        {
            width  = -1;
            height = -1;
            bytesPerLine  = -1;

            frameNumber = -1;
            frameTime   = -1;

            imageRGB = 0;
            imageGreyscale = 0;

            position = -1;

            isInit = false;

            features = 0;

            //
            faceImage = 0;
            prob = -1;

            isSample = false;
        }

        ~QxImageFramePrivate()
        {
            imageRGB = 0;
            imageGreyscale = 0;
            features = 0;

            faceImage = 0;
        }
};

QxImageFrame::QxImageFrame()
{
    data = new QxImageFramePrivate;
}

QxImageFrame::~QxImageFrame()
{
    if( data != 0)
    {
        this->clear();
        delete data;
        data = 0;
    }
}

//************************************************************
//
//  System functions start
//
//************************************************************

void QxImageFrame::copyTo(QxImageFrame *dst)
{
    if( (data) && (dst) )
    {
        dst->imageFromData (data->width, data->height, data->imageRGB, data->bytesPerLine);
        dst->setFrameNumber(data->frameNumber);
        dst->setFrameTime  (data->frameTime);
    }
}

void QxImageFrame::copyHeader(QxImageFrame *dst)
{
    if( (data) && (dst) )
    {
        dst->data->isInit = true;

        dst->data->bytesPerLine = data->bytesPerLine;

        dst->data->height = data->height;
        dst->data->width  = data->width;

        dst->data->prob = data->prob;

        dst->data->frameNumber = data->frameNumber;

        dst->data->frameTime = data->frameTime;

        dst->data->faceRect  = data->faceRect;
    }
}

bool QxImageFrame::imageFromData(int width, int height, unsigned char *bits, int bytesPerLine, ifPixelFormat format)
{
    bool result = false;
    int invert = false;
    int pos = 0;
    int step;
    int size;
    if( (data != 0) && (bits != 0) && ( bytesPerLine > 0) && (width*height>0))
    {
        bool isValid = true;

        if( data->isInit )
        {
            if( data->bytesPerLine != bytesPerLine )
            {
                isValid = false;
            }

            if( data->width != width )
            {
                isValid = false;
            }

            if( data->height != height )
            {
                isValid = false;
            }
        }
        else
        {
            data->width = width;
            data->height = height;
            data->bytesPerLine = bytesPerLine;
        }

        if( isValid )
        {
            size = data->bytesPerLine*data->height;
            if( !data->imageRGB )
            {
                data->imageRGB = new unsigned char [size];
            }

            {
                switch( format )
                {
                    default:
                    case pfRGB:
                        step = 3;
                        invert = false;
                        pos = 0;
                    break;
                    case pfBGR:
                        step = 3;
                        invert = true;
                        pos = 0;
                    break;
                    case pfRGBA:
                        step = 4;
                        invert = false;
                        pos = 0;
                    break;
                    case pfBGRA:
                        step = 4;
                        invert = true;
                        pos = 0;
                    break;
                    case pfARGB:
                        step = 4;
                        invert = false;
                        pos = 1;
                    break;
                    case pfABGR:
                        step = 4;
                        invert = true;
                        pos = 1;
                    break;
                };
            }

            size /= step;
            if( data->imageRGB )
            {
                if( !invert )
                {
                    for(int i=0; i<size; i++)
                    {

                        {
                            data->imageRGB[pos+0] = bits[pos+0];
                            data->imageRGB[pos+1] = bits[pos+1];
                            data->imageRGB[pos+2] = bits[pos+2];
                            pos+=step;
                        }
                    }
                }
                else
                {
                    for(int i=0; i<height; i++)
                    {
                        for(int j=0; j<width; ++j)
                        {
                            data->imageRGB[pos+2] = bits[pos+0];
                            data->imageRGB[pos+1] = bits[pos+1];
                            data->imageRGB[pos+0] = bits[pos+2];
                            pos+=step;
                        }
                        pos = (i+1)*bytesPerLine;
                    }
                }
                data->isInit = true;
                result = true;
            }
        }
    }
    return result;
}

void QxImageFrame::createGrayscale()
{
    if( data != 0)
    {
        if( (data->imageRGB) && (data->isInit))
        {
            int size  = data->width;
            if ( data->width%4 )
            {
                size += 4-data->width%4;
            }

            if( !data->imageGreyscale )
                data->imageGreyscale = new unsigned char[size*data->height];

            cv::Mat src(data->height, data->width, CV_8UC3, data->imageRGB, data->bytesPerLine);
            cv::Mat dst(data->height, data->width, CV_8UC1, data->imageGreyscale, size);
            cv::cvtColor(src, dst, CV_RGB2GRAY );
        }
    }
}

unsigned char *QxImageFrame::getImageRGBBits()
{
    unsigned char *result = 0;
    if( data )
    {
        result = data->imageRGB;
    }
    return result;
}

void QxImageFrame::putImageRGBInto(unsigned char *bits, int step, int bpp, int start)
{
    if( (data) && (bits))
    {
        if( data->imageRGB )
        {
            memcpy(bits, data->imageRGB, data->bytesPerLine*data->height);
        }
    }
}

unsigned char *QxImageFrame::getImageGrayscaleBits()
{
    unsigned char *result=0;
    if( data )
    {
        if( !data->imageGreyscale )
        {
            this->createGrayscale();
        }
        result = data->imageGreyscale;
    }
    return result;
}

void  QxImageFrame::putImageGrayscaleInto(unsigned char *bits, int step, int bpp, int start)
{
    if( (data) && (bits))
    {
        unsigned char *gray = this->getImageGrayscaleBits();
        if( gray )
        {
            int size  = data->width;
            if ( data->width%4 )
            {
                size += 4-data->width%4;
            }

            int psize = bpp*data->width;
            if( (bpp*data->width)%4 )
            {
                psize+= 4-(bpp*data->width)%4;
            }

            int pos = 0;
            int pix = 0;

            for(int i=0; i<data->height; ++i)
            {
                pix = i*(size);
                pos = i*(psize);
                for( int k = 0; k<data->width; k++ )
                {
                    for( int j=0; j<bpp; ++j)
                    {
                        bits[pos+j] =gray[pix];
                    }

                    pix++;
                    pos+=bpp;
                }
            }
        }
    }
}

void QxImageFrame::clear()
{
    if( data != 0)
    {
        this->reset();
        this->clearFaceImage();

        if( data->imageRGB != 0)
        {
            delete []data->imageRGB;
            data->imageRGB = 0;
        }
        if( data->imageGreyscale != 0)
        {
            delete []data->imageGreyscale;
            data->imageGreyscale = 0;
        }

        if( data->features )
        {
            delete data->features;
            data->features = 0;
        }
    }
}

void QxImageFrame::reset()
{
    if( data != 0)
    {
        data->isInit = false;
        data->width  = -1;
        data->height = -1;
        data->bytesPerLine =-1;

        data->frameNumber = -1;
        data->frameTime   = -1;

        data->prob = -1;
        if( data->features )
        {
            data->features->clearAndErase();
        }
    }
}

void QxImageFrame::setImageFeatureList(QxImageFeatureList *features)
{
    if( data )
    {
        if( data->features )
        {
            delete data->features;
            data->features = 0;
        }
        data->features = features;
    }
}

QxImageFeatureList *QxImageFrame::getImageFeatureList()
{
    QxImageFeatureList *result = 0;
    if( data )
    {
        if( !data->features )
        {
            data->features = new QxImageFeatureList;
        }
        result = data->features;
    }
    return result;
}

void QxImageFrame::unloadFromMemory()
{
    if( data != 0)
    {
        if( data->imageRGB != 0)
        {
            delete []data->imageRGB;
            data->imageRGB = 0;
        }

        if( data->imageGreyscale )
        {
            delete []data->imageGreyscale;
            data->imageGreyscale = 0;
        }
    }
}

void QxImageFrame::resetFeatures()
{
    if( data != 0)
    {
        if( data->features != 0)
        {
            data->features->clearAndErase();
        }
    }
}

int QxImageFrame::width()
{
    int result = -1;
    if( data != 0)
    {
        result = data->width;
    }
    return result;
}

int QxImageFrame::height()
{
    int result = -1;
    if( data != 0)
    {
        result = data->height;
    }
    return result;
}

int QxImageFrame::bytesPerLine()
{
    int result = -1;
    if( data )
    {
        result = data->bytesPerLine;
    }
    return result;
}

bool QxImageFrame::isInit()
{
    bool result = false;
    if( data != 0)
    {
        result = data->isInit;
    }
    return result;
}

bool QxImageFrame::isValid()
{
    bool result = false;
    if( data != 0)
    {
        result = data->isInit;

        if( data->height <= 0 )
        {
            result = false;
        }

        if( data->width  <= 0 )
        {
            result = false;
        }

        if( data->imageRGB == 0)
        {
            result = false;
        }

        /*if( data->imageGreyscale ==0 )
        {
            result = false;
        }*/
    }
    return result;
}

void  QxImageFrame::setFrameTime(float time)
{
    if( data != 0)
    {
        if( time != data->frameTime)
        {
            data->frameTime = time;
        }
    }
}

float QxImageFrame::getFrameTime()
{
    float result = -1;
    if( data != 0)
    {
        result = data->frameTime;
    }
    return result;
}

void  QxImageFrame::setFrameNumber(int frame)
{
    if( data != 0)
    {
        if( frame != data->frameNumber)
        {
            data->frameNumber = frame;
        }
    }
}

int   QxImageFrame::getFrameNumber()
{
    int result = -1;
    if( data != 0)
    {
        result = data->frameNumber;
    }
    return result;
}

void  QxImageFrame::setFramePosition( int val)
{
    if( data != 0)
    {
        if( data->position != val)
        {
            data->position = val;
        }
    }
}

int   QxImageFrame::getFramePosition()
{
    int result = -1;
    if( data != 0)
    {
        result = data->position;
    }
    return result;
}
//############################################################
//
//  System functions end
//
//############################################################


//************************************************************
//
//  Additionl functions start
//
//************************************************************

void QxImageFrame::setProb(float prob)
{
    if( data )
    {
        data->prob = prob;
    }
}

float QxImageFrame::getProb()
{
    float result = -1;
    if( data )
    {
        result = data->prob;
    }
    return result;
}

void  QxImageFrame::setFaceRect(QRect &rct)
{
    if( data )
    {
        this->clearFaceImage();
        data->faceRect = rct;
    }
}

QRect QxImageFrame::getFaceRect()
{
    QRect result;
    if( data )
    {
        result = data->faceRect;
    }
    return result;
}

/*------------------------------------------------------------------------*/

void QxImageFrame::setFaceImage(unsigned char *img)
{
    if( data )
    {
        if( !data->faceRect.isNull() )
        {
            int size = data->faceRect.width();
            int bytes;
            if( size%4 )
            {
                size+= (4-size%4);
            }
            bytes = size*data->faceRect.height();

            data->faceImage = new unsigned char[bytes];
            memcpy(data->faceImage, img, bytes);
        }
    }
}

unsigned char *QxImageFrame::getFaceImage()
{
    unsigned char *result=0;
    if( data )
    {
        result = data->faceImage;
    }
    return result;
}

void QxImageFrame::clearFaceImage()
{
    if( data )
    {
        delete []data->faceImage;
        data->faceRect = QRect();
    }
}

void QxImageFrame::setIsSample(bool on)
{
    data->isSample = on;
}

bool QxImageFrame::getIsSample()
{
    return data->isSample;
}

//------------------------------------------------------------
//  Additionl functions end
//------------------------------------------------------------



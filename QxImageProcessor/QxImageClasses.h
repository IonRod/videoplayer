#ifndef QXIMAGECLASSES_H
#define QXIMAGECLASSES_H

class QxImageFrame;
class QxImageManager;
class QxFaceRecognizer;
class QxDataBase;
class QxImageFeature;
class QxImageFeatureList;
class QxFeatureDetector;
#endif // QXIMAGECLASSES_H

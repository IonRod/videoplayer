#ifndef QXIMAGEPAINTER_H
#define QXIMAGEPAINTER_H

#include <Qt>
#include <QPainter>
#include <QImage>


#include "QxImageFrame.h"
#include "QxImageFeature.h"

class QxImagePainterPrivate;

class QxImagePainter
{
public:
    QxImagePainter();
    ~QxImagePainter();


public:
    void drawCircleKeypoints( QImage &image, QxImageFrame *frame);
    void drawCircleKeypoints( QImage &image, QxImageFrame *frame, QxImageFeatureList *features, QList<QColor> &colors );
protected:
    QxImagePainterPrivate *data;
};

#endif // QXIMAGEPAINTER_H

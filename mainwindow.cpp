#include <QFileDialog>
#include <QMessageBox>
#include <QPainter>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    manager.setIsVideo(true);
    printf("Starting up\n");

    connect(ui->horizontalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(framePositionChanged(int)));

    connect( &timer, SIGNAL(timeout()), this, SLOT(step()));
    connect( ui->btnPause, SIGNAL(clicked()), this, SLOT(Pause()));
    connect( ui->btnPlay, SIGNAL(clicked()), this, SLOT(Play()));
    this->Pause();
    isPlay = false;
    isLeftDown = false;
    isRightDown = false;

    base = 0;

    ui->lblTimeLine->setAutoFillBackground(false);
    ui->lblTimeLine->setAttribute(Qt::WA_PaintOutsidePaintEvent);
    ui->lblTimeLine->installEventFilter(this);

    minLevel = 0;

    isDrawTo = true;
    drawToWidget = 0;

    decoder = new QVideoDecoder;

    isRender = rsNone;

    fps     = 0;
    prevFps = 0;
    lastFpsTime.start();

    propagateIterations = 1;
}

MainWindow::~MainWindow()
{
    if( decoder )
    {
        delete decoder;
        decoder = 0;
    }
    delete ui;
}

/******************************************************************************
*******************************************************************************
*
*******************************************************************************
******************************************************************************/

void MainWindow::loadVideo(QString fileName)
{
   this->Pause();
   if( !decoder )
       return;
       decoder->openFile(fileName);

   if( decoder->isOk()==false)
   {
      QMessageBox::critical(this,"Error","Error loading the video");
      return;
   }

   manager.clear();
   imageFrame.clear();

   ui->spinBoxFrame_2->blockSignals(true);
       ui->spinBoxFrame_2->setMinimum(0);
       ui->spinBoxFrame_2->setMaximum( int(decoder->getVideoLengthMs()) );
       ui->spinBoxFrame_2->setSingleStep(40);
       ui->lblFrameNumber_2->setText( QString().sprintf("%i", int(decoder->getVideoLengthMs())));
   ui->spinBoxFrame_2->blockSignals(false);

   ui->horizontalSlider_2->blockSignals(true);
       ui->horizontalSlider_2->setMinimum(0);
       ui->horizontalSlider_2->setMaximum( int(decoder->getVideoLengthMs()) );
       ui->horizontalSlider_2->setSingleStep(1);
   ui->horizontalSlider_2->blockSignals(false);



   QxImageFrame *frm = this->getFrameFromTime(20);
   if( base )
   {
       int tmp  = frm->width();

       if ( frm->width()%4 )
       {
           tmp += 4-frm->width()%4;
       }
       base->updateDetector(frm->width(), frm->height(), tmp);
   }
   this->framePositionChanged(20);

}

void MainWindow::errLoadVideo()
{
   QMessageBox::critical(this,"Error","Load a video first");
}

bool MainWindow::checkVideoLoadOk()
{
   if( !decoder )
       return false;
   if(decoder->isOk()==false)
   {
      errLoadVideo();
      return false;
   }
   return true;
}

void MainWindow::displayFrame()
{

    QPainter painter(ui->lblTimeLine);
    QRect rect = ui->lblTimeLine->rect();
    painter.fillRect(rect, QColor(0,0,0));

    if( rect.width() <=0 )
    {
        return;
    }

    QxImageFrame *frame;

    painter.save();
    if( decoder )
    {
        float length = decoder->getVideoLengthMs();

        float segment = length / float( rect.width() );

        float time = 0;
        float segTime;

        int r;
        int b;
        int g;

        float size = float( rect.width() )/length;

        int pix = 0;
        int green = 0;
        int red = 0;
        int total = 0;


        while (time<length)
        {
            segTime = 0;

            r = 10;
            g = 10;
            b = 10;

            while(segTime<segment)
            {
                segTime+=FRAME_TIME;
                frame = manager.getImageByTime(time+segTime);
                if( frame )
                {
                    if( frame->getProb() >= 0  )
                    {
                        total++;

                        if( frame->getProb() >= base->getMinLevel() )
                        {
                            r = 0;
                            g = 30+frame->getProb()*210+2;

                            green ++;
                        }
                        else
                        {
                            red ++;
                            r = 30+(100-frame->getProb()*100+2);
                            g = 0;
                            b = 0;
                        }
                    }
                    else
                    {
                        r = 50;
                        g = 50;
                        b = 50;
                    }
                }
            }

            time+=segment;

            painter.setPen(QColor(r, g, b));

            painter.drawLine(pix, 10, pix, rect.height()-10);
            pix++;

        }

        painter.setPen(QColor(200, 200, 200));
        if( length > 0 )
        {
            painter.drawLine(passedTime*size, rect.height() - 10, passedTime*size-5, rect.height());
            painter.drawLine(passedTime*size, rect.height() - 10, passedTime*size+5, rect.height());
        }

        switch( isRender )
        {
            default:
            case rsNone:
                painter.setPen(QColor(200, 200, 255));
                painter.drawText(QRect(0,0,500,100), "Frames with person: "+QString().setNum(green)+" Frames without person: "+QString().setNum(red)+" Total frames count: "+QString().setNum(total));
                if( total > 0)
                painter.drawText(QRect(0, 15,500,100), "Persent of frames with person: "+QString().setNum(float(green)/float(total)));
            break;

            case rsSearching:
                painter.setPen(QColor(200, 255, 200));
                painter.drawText(rect, QString("Searching progress: %5").arg(renderProgress));
            break;

            case rsFlow:
                painter.setPen(QColor(255, 200, 200));
                painter.drawText(rect, QString("Iteration %1 Progress: %5").arg(renderIteration).arg(renderProgress));
            break;
        }
    }
    painter.restore();
}

QxImageFrame *MainWindow::getPrevFrame()
{
    QxImageFrame *result = 0;
    this->framePositionChanged(passedTime-FRAME_TIME);
    return result;
}

QxImageFrame *MainWindow::getNextFrame()
{
    QxImageFrame *result = 0;
    this->framePositionChanged(passedTime+FRAME_TIME);
    return result;
}

QxImageFrame *MainWindow::getFrameFromTime(int time)
{
    if( (!decoder) || (time < 0))
        return 0;

    QxImageFrame *result = 0;
    QxImageFrame *temp   = 0;

    temp = manager.getImageByTime(time);

    if( !temp )
    {
        bool res = false;
        if( (time>=passedTime) && ((time - FRAME_TIME)<=passedTime) )
        {
            res = decoder->seekNextFrame();

            if( res )
            {
                if( fabs(time - decoder->getFrameTime()) > FRAME_TIME )
                {
                    res = false;
                }
            }
        }

        if (!res)
            res = decoder->seekMs(time);

        if( res )
        {
            temp = new QxImageFrame;
            if( decoder->getFrame(*temp) )
            {
                temp->setFrameTime(time);
                manager.addImage(temp);

                result = temp;
                temp   = 0;
            }
            else
            {
                delete temp;
                temp = 0;
            }
        }
    }
    else
    {
        result = temp;
        temp = 0;
        if( !result->getImageRGBBits() )
        {
           if( decoder->seekMs(time) )
           {
               decoder->getFrame(*result);
               manager.incInMemoryCount();
               manager.val(result->getFramePosition());
           }
        }
    }
    return result;
}


//

void MainWindow::proceedFrame()
{

    QxImageFrame *currImage;
    QxImageFrame *prevImage;

    int currentId = passedTime;

    currImage = this->getFrameFromTime(currentId);
    prevImage = this->getFrameFromTime(currentId-40);

    if( currImage )
    {
        if( base )
        {
            if( currImage->getProb() < base->getMinLevel())
            {
                base->detect(currImage, prevImage);
            }
        }
        if( !imageFrame.imageFromData(currImage->width(), currImage->height(), currImage->getImageRGBBits(), currImage->bytesPerLine() ) )
        {
            imageFrame.clear();
            imageFrame.imageFromData(currImage->width(), currImage->height(), currImage->getImageRGBBits(), currImage->bytesPerLine() );
        }
        currImage->copyHeader(&imageFrame);
    }
}

void MainWindow::newFrameLoaded()
{
    if( !decoder )
        return;

    ui->spinBoxFrame_2->blockSignals(true);
        ui->spinBoxFrame_2->setMinimum(0);
        ui->spinBoxFrame_2->setMaximum( int(decoder->getVideoLengthMs()) );
        ui->spinBoxFrame_2->setSingleStep(40);
        ui->lblFrameNumber_2->setText( QString().sprintf("%i",  int(decoder->getVideoLengthMs())));
    ui->spinBoxFrame_2->blockSignals(false);

    ui->horizontalSlider_2->blockSignals(true);
        ui->horizontalSlider_2->setMinimum(0);
        ui->horizontalSlider_2->setMaximum( int(decoder->getVideoLengthMs()) );
        ui->horizontalSlider_2->setSingleStep(1);
    ui->horizontalSlider_2->blockSignals(false);


    ui->spinBoxFrame_2->blockSignals(true);
    ui->horizontalSlider_2->blockSignals(true);

    ui->spinBoxFrame_2->setValue(passedTime);

    ui->horizontalSlider_2->setValue(passedTime);

    ui->spinBoxFrame_2->blockSignals(false);
    ui->horizontalSlider_2->blockSignals(false);

    int mtime = float(passedTime)/1000.;
    int min = float(mtime)/60.;
    int sec = mtime - min*60;

    ui->lblTime->setText(QString().sprintf("%2i",min)+":"+QString().sprintf("%2i",sec));

    ui->lblTimeLine->repaint();
    if( drawToWidget )
        if( isDrawTo )
            drawToWidget->repaint();
}

void MainWindow::framePositionChanged(int val)
{
    QxImageFrame *frame = this->getFrameFromTime(val);

    if( !frame )
    {
        this->Pause();
    }
    else
    {
        passedTime = val;
        this->proceedFrame();
        this->newFrameLoaded();
    }
}

//
void MainWindow::Play()
{
    if( isPlay == false)
    {
        fps = 0;
        prevFps = 0;
        lastFpsTime.restart();

        isPlay = true;
        timer.start(0);
        prevTime.restart();
    }
}

void MainWindow::Pause()
{
    isPlay = false;
    timer.stop();
}

void MainWindow::step()
{
    this->timer.stop();
    if( isPlay )
    {
        this->timer.stop();

        int dT = prevTime.restart();


        prevFps++;
        this->framePositionChanged(passedTime + FRAME_TIME);

        if( lastFpsTime.elapsed() >= 1000 )
        {
            fps = prevFps;
            prevFps = 0;
            lastFpsTime.restart();
        }
        this->timer.start();
    }
}

void MainWindow::setDatabase( QxDataBase *_base)
{
    this->base = _base;
}

void MainWindow::setDrawWidget(QWidget *widget)
{
    drawToWidget = widget;
}

void MainWindow::setIsDraw( bool val)
{
    isDrawTo = val;
}

bool MainWindow::getIsDraw()
{
    return isDrawTo;
}

QxImageFrame *MainWindow::getFrame()
{
    return &imageFrame;
}

float MainWindow::getFps()
{
    return fps;
}

//
/******************************************************************************
*******************************************************************************
* events
*******************************************************************************
******************************************************************************/

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
        break;

        default:
        break;
    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *ev)
{
    bool result = false;
    if( obj == ui->lblTimeLine )
    {
        switch( ev->type())
        {
            case QEvent::Paint:
                this->displayFrame();
                result = true;
            break;

            case QEvent::MouseButtonPress:
                QMouseEvent *evt = (QMouseEvent *)ev;

                int x = evt->pos().x();

                float pos = float(x)/float(ui->lblTimeLine->width());

                this->framePositionChanged( this->decoder->getVideoLengthMs() * pos);

                result = true;
            break;
        }
    }
    return result;
}

void MainWindow::on_pushButton_clicked()
{
    int val = passedTime;
    QxImageFrame *frame=0;
    QxImageFrame *prevFrame=0;

    this->Pause();

    if( this->base )
    {
        int length = decoder->getVideoLengthMs();
        int count = float(ui->spinBox->value())*25;

        {
            int start, stop;

            start = -count;
            stop = count;

            if( val + start*FRAME_TIME < 0 )
            {
                start = -val/FRAME_TIME+2;
            }


            if( val + stop*FRAME_TIME > length )
            {
                stop = (length-val)/FRAME_TIME-2;
            }

            QTime time;
            QTime cycle;
            cycle.start();
            time.start();

            std::cout<<"Frames to load: "<<stop-start<<" Time to load: "<<FRAME_TIME*(stop-start)<<std::endl;

            isRender = rsSearching;
            renderProgress = 0;


            int used[stop-start+10];
            float ppos;

            for( int i=start; i<stop; ++i )
            {
                used[i]=0;
            }

            int j;

            renderProgress  = -100;
            isRender        = rsFlow;
            renderIteration = 0;

            bool changed = false;
            bool framechanged;

            prevFrame = 0;

            QTime totalTime;
            totalTime.start();

            for(renderIteration = 1; renderIteration <= this->propagateIterations; renderIteration++)
            {
                for( int i=start; i<stop; ++i )
                {
                    if(50.*float(i-start)/float(stop-start) > renderProgress + 5 )
                    {
                        renderProgress  = 50.*float(i-start)/float(stop-start);
                        ui->lblTimeLine->repaint();
                        QCoreApplication::processEvents();
                    }

                    frame = this->getFrameFromTime(val + i*FRAME_TIME);

                    if( frame )
                    {
                        framechanged = false;

                        if( frame->getProb() < base->getMinLevel() )
                        {
                            base->detect(frame, prevFrame);
                            framechanged = true;
                        }

                        if( frame->getProb() >= base->getMinLevel() )
                        {
                            if( framechanged )
                            {
                                changed = true;
                            }
                            prevFrame = frame;
                        }
                        else
                        {
                            prevFrame = 0;
                        }
                    }
                }

                for( int i=stop-1; i>=start; --i )
                {
                    if( (50.+ 50.*float(stop-i)/float(stop-start)) > (renderProgress + 5) )
                    {
                        renderProgress  =50.+ 50.*float(stop-i)/float(stop-start);
                        ui->lblTimeLine->repaint();
                        QCoreApplication::processEvents();
                    }

                    frame = this->getFrameFromTime(val + i*FRAME_TIME);

                    if( frame )
                    {
                        framechanged = false;

                        if( frame->getProb() < base->getMinLevel() )
                        {
                            base->detect(frame, prevFrame);
                            framechanged = true;
                        }

                        if( frame->getProb() >= base->getMinLevel() )
                        {
                            if( framechanged )
                            {
                                changed = true;
                            }
                            prevFrame = frame;
                        }
                        else
                        {
                            prevFrame = 0;
                        }
                    }
                }

                if( !changed )
                {
                    break;
                }
            }
            renderIteration++;


            if(renderIteration>this->propagateIterations)
                renderIteration=this->propagateIterations;

            QMessageBox::warning(this, "Finished", QString("Total flow iterations: ")+QString().setNum(renderIteration)+QString(" from: ")
                                 +QString().setNum(this->propagateIterations)+QString("\n\rTotal time: ")+QString().setNum(totalTime.elapsed()/1000));
        }

        isRender = rsNone;
        ui->lblTimeLine->repaint();
    }

    passedTime = val;
}

//
/******************************************************************************
*******************************************************************************
* Widget slots
*******************************************************************************
******************************************************************************/

void MainWindow::on_checkBox_clicked(bool checked)
{
    if( base )
        base->setIsLearning( checked );
}

void MainWindow::on_spinBoxFrame_2_editingFinished()
{
    this->framePositionChanged( ui->spinBoxFrame_2->value() );
}

void MainWindow::on_doubleSpinBox_editingFinished()
{
    if( base )
        base->setMinLevel(ui->doubleSpinBox->value());
}

void MainWindow::on_doubleSpinBox_2_valueChanged(double arg1)
{
    if( base )
        base->setLearningLevel(arg1);
}


void MainWindow::on_actionSave_synthetic_video_triggered()
{

}


void MainWindow::on_labelVideoFrame_linkActivated(const QString &link)
{

}

void MainWindow::on_actionCache_triggered()
{

}

void MainWindow::on_actionReal_length_triggered()
{

}

void MainWindow::on_pushButtonNextFrame_2_clicked()
{
    this->getNextFrame();
}

void MainWindow::on_pushButtonPrevFrame_2_clicked()
{
    this->getPrevFrame();
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_actionLoad_video_triggered()
{
    // Prompt a video to load
   QString fileName = QFileDialog::getOpenFileName(this, "Load Video",QString());
   if(!fileName.isNull())
   {
      loadVideo(fileName);
   }
}

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    propagateIterations = arg1;
}

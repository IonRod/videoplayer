

DEFINES += MAC

TARGET = VideoPlayer
TEMPLATE = app

SOURCES += \
    mainwindow.cpp \
    QxImageProcessor/QxImageManager.cpp \
    QxImageProcessor/QxImageFrame.cpp \
    QxImageProcessor/QxFaceRecognizer.cpp \
    QxImageProcessor/QxDataBase.cpp \
    QwImageProcessor/QwFaceRecognizer.cpp \
    QxImageProcessor/QxImageFeature.cpp \
    QxImageProcessor/QxFeatureDetector.cpp \
    QxImageProcessor/QxImagePainter.cpp\
    QxImageProcessor/QxImageFilter.cpp \
    main.cpp \
    cio.cpp \
    QxImageProcessor/tld/VarianceFilter.cpp \
    QxImageProcessor/tld/TLDUtil.cpp \
    QxImageProcessor/tld/TLD.cpp \
    QxImageProcessor/tld/NNClassifier.cpp \
    QxImageProcessor/tld/MedianFlowTracker.cpp \
    QxImageProcessor/tld/ForegroundDetector.cpp \
    QxImageProcessor/tld/EnsembleClassifier.cpp \
    QxImageProcessor/tld/DetectorCascade.cpp \
    QxImageProcessor/tld/DetectionResult.cpp \
    QxImageProcessor/tld/Clustering.cpp \
    QxImageProcessor/tld/tracker/median.cpp \
    QxImageProcessor/tld/tracker/lk.cpp \
    QxImageProcessor/tld/tracker/fbtrack.cpp \
    QxImageProcessor/tld/tracker/bb_predict.cpp \
    QxImageProcessor/tld/tracker/bb.cpp \
    QxImageProcessor/cvblobs/ComponentLabeling.cpp \
    QxImageProcessor/cvblobs/BlobResult.cpp \
    QxImageProcessor/cvblobs/BlobProperties.cpp \
    QxImageProcessor/cvblobs/BlobOperators.cpp \
    QxImageProcessor/cvblobs/BlobContour.cpp \
    QxImageProcessor/cvblobs/blob.cpp \
    ../../../../Projects/FFMpeg/QTFFmpegWrapper/QVideoDecoder.cpp
HEADERS += mainwindow.h \
    QxImageProcessor/QxImageManager.h \
    QxImageProcessor/QxImageFrame.h \
    QxImageProcessor/QxFaceRecognizer.h \
    QxImageProcessor/QxImageClasses.h \
    QxImageProcessor/QxDataBase.h \
    QwImageProcessor/QwFaceRecognizer.h \
    Global.h \
    QxImageProcessor/QxImageFeature.h \
    QxImageProcessor/QxFeatureDetector.h \
    QxImageProcessor/QxImagePainter.h\
    QxImageProcessor/QxImageFilter.h \
    Global.h \
    cio.h \
    QxImageProcessor/tld/VarianceFilter.h \
    QxImageProcessor/tld/TLDUtil.h \
    QxImageProcessor/tld/TLD.h \
    QxImageProcessor/tld/NormalizedPatch.h \
    QxImageProcessor/tld/NNClassifier.h \
    QxImageProcessor/tld/MedianFlowTracker.h \
    QxImageProcessor/tld/IntegralImage.h \
    QxImageProcessor/tld/ForegroundDetector.h \
    QxImageProcessor/tld/EnsembleClassifier.h \
    QxImageProcessor/tld/DetectorCascade.h \
    QxImageProcessor/tld/DetectionResult.h \
    QxImageProcessor/tld/Clustering.h \
    QxImageProcessor/tld/tracker/median.h \
    QxImageProcessor/tld/tracker/lk.h \
    QxImageProcessor/tld/tracker/fbtrack.h \
    QxImageProcessor/tld/tracker/bb_predict.h \
    QxImageProcessor/tld/tracker/bb.h \
    QxImageProcessor/cvblobs/ComponentLabeling.h \
    QxImageProcessor/cvblobs/BlobResult.h \
    QxImageProcessor/cvblobs/BlobProperties.h \
    QxImageProcessor/cvblobs/BlobOperators.h \
    QxImageProcessor/cvblobs/BlobLibraryConfiguration.h \
    QxImageProcessor/cvblobs/BlobContour.h \
    QxImageProcessor/cvblobs/blob.h
FORMS += mainwindow.ui \
    QwImageProcessor/QwFaceRecognizer.ui
RESOURCES +=

DEFINES += DEVELMODE


# ##############################################################################
# OpenCV: START OF CONFIGURATION
# ##############################################################################

win32{
    OPENCV_PATH = $$quote(C:/OpenCVLib/install)

    INCLUDEPATH += $$OPENCV_PATH/include
    LIBS += -L$$OPENCV_PATH/lib \
            -lopencv_core231 \
            -lopencv_imgproc231 \
            -lopencv_video231 \
            -lopencv_features2d231 \
            -lopencv_calib3d231 \
            -lopencv_highgui231 \
            -lopencv_objdetect231 \
            -lopencv_legacy231
}

macx{
    LIBS += -lopencv_core \
            -lopencv_imgproc \
            -lopencv_video \
            -lopencv_features2d \
            -lopencv_calib3d \
            -lopencv_highgui \
            -lopencv_objdetect \
            -lopencv_legacy
}

QMAKE_LFLAGS += -fopenmp

QMAKE_LIBS+= -lgomp -lpthread
QMAKE_CXXFLAGS+=-fopenmp

LIBS += -LC:/QtSDK/mingw/lib/gcc/mingw32/4.4.0/libgomp.a

# ##############################################################################
# FFMPEG: START OF CONFIGURATION
# ##############################################################################

    QTFFMPEGWRAPPER_SOURCE_PATH = $$quote(D:/Projects/FFMpeg)/QTFFmpegWrapper
    FFMPEG_LIBRARY_PATH = $$quote(D:/Projects/FFMpeg)/ffmpeg_lib_win32
    FFMPEG_INCLUDE_PATH =$$quote(D:/Projects/FFMpeg)/QTFFmpegWrapper

    # Set list of required FFmpeg libraries
    LIBS += -lavutil \
        -lavcodec \
        -lavformat \
        -lswscale

    # Add the path
    LIBS += -L$$FFMPEG_LIBRARY_PATH
    INCLUDEPATH += QVideoEncoder
    INCLUDEPATH += $$FFMPEG_INCLUDE_PATH

    # Requied for some C99 defines
    DEFINES += __STDC_CONSTANT_MACROS

# ##############################################################################
# FFMPEG: END OF CONFIGURATION
# ##############################################################################

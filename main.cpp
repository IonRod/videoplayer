/*
   QTFFmpegWrapper Demo
   Copyright (C) 2009,2010:
         Daniel Roggen, droggen@gmail.com

   All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <QtGui/QApplication>


#include <opencv2/core/core.hpp>

#include "cio.h"
#include "mainwindow.h"
#include "QwImageProcessor/QwFaceRecognizer.h"

#include "QxImageProcessor/QxDataBase.h"
#include "QxImageProcessor/QxFaceRecognizer.h"
#include "QxImageProcessor/QxImageClasses.h"
#include "QxImageProcessor/QxImageFrame.h"
#include "QxImageProcessor/QxImageManager.h"

int main(int argc, char *argv[])
{


    FILE *file = 0;
    file = fopen("threads.txt", "r");
    if( file )
    {
        int threads = 0;
        fscanf(file, "%i", &threads);

        if( threads > 0)
            omp_set_num_threads(threads);
    }
    QApplication a(argc, argv);
    //ConsoleInit();

    QxImageManager images;
    QxImageManager video;
    QxFaceRecognizer recognizer;

    QxDataBase base;
    QxFeatureDetector detector;

    QwFaceRecognizer imageWindow;

    MainWindow w;

    base.setFeatureDetector( &detector );

    imageWindow.setImages( &images);
    imageWindow.setDatabase( &base);
    imageWindow.setFeatureDetector( & detector);

    imageWindow.applyClicked();

    w.setDatabase( &base );
    w.move(0, 0);
    imageWindow.move(w.width()+50, 0);

    imageWindow.setMainWindow( &w );
    w.show();
    imageWindow.show();


    return a.exec();
}

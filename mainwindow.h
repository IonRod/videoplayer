/*
   QTFFmpegWrapper Demo
   Copyright (C) 2009,2010:
         Daniel Roggen, droggen@gmail.com

   All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QMouseEvent>

#include <QTime>
#include <QTimer>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/video/background_segm.hpp>

#include "QxImageProcessor/QxImageManager.h"
#include "QxImageProcessor/QxImageFrame.h"
#include "QxImageProcessor/QxDataBase.h"

#include "QVideoDecoder.h"


#define FRAME_TIME 40
namespace Ui {
    class MainWindow;
}

enum renderState{
    rsNone,
    rsSearching,
    rsFlow
};

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    QVideoDecoder *decoder;


    void changeEvent(QEvent *e);
    // Decoder demo
    void displayFrame();
    void loadVideo(QString fileName);
    void errLoadVideo();    
    bool checkVideoLoadOk();

    QxImageFrame *getPrevFrame();
    QxImageFrame *getNextFrame();
    QxImageFrame *getFrameFromTime(int time);

    //Player
    int  passedTime;
    bool isPlay;
    renderState isRender;
    int renderIteration;
    float renderProgress;

    bool isDrawTo;
    QWidget *drawToWidget;

    QTime  prevTime;
    QTimer timer;

    float minLevel;
    //
    QxImageManager manager;
    QxImageFrame   imageFrame;
    QxDataBase     *base;

    float fps;
    float prevFps;
    QTime lastFpsTime;

    int propagateIterations;
protected:
    bool isLeftDown;
    bool isRightDown;
    QPoint startPosition;

    virtual bool eventFilter(QObject *obj, QEvent *ev);
private:
    Ui::MainWindow *ui;

public:
    void proceedFrame();
    void newFrameLoaded();
public slots:
    void framePositionChanged(int val);
public slots:
    void Play();
    void Pause();
    void step();
public:
    void setDatabase( QxDataBase *_base);
    void setDrawWidget(QWidget *widget);
    void setIsDraw( bool val);
    bool getIsDraw();
    QxImageFrame *getFrame();
    float getFps();
private slots:
    void on_actionSave_synthetic_video_triggered();
    void on_actionLoad_video_triggered();
    void on_actionQuit_triggered();
    void on_labelVideoFrame_linkActivated(const QString &link);
    void on_actionCache_triggered();
    void on_actionReal_length_triggered();
    void on_pushButtonNextFrame_2_clicked();
    void on_pushButtonPrevFrame_2_clicked();
    void on_pushButton_clicked();
    void on_checkBox_clicked(bool checked);
    void on_spinBoxFrame_2_editingFinished();
    void on_doubleSpinBox_editingFinished();
    void on_doubleSpinBox_2_valueChanged(double arg1);
    void on_spinBox_2_valueChanged(int arg1);
};

#endif // MAINWINDOW_H

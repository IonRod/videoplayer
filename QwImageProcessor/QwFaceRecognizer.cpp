#include "QwFaceRecognizer.h"
#include "ui_QwFaceRecognizer.h"

class QwFaceRecognizerPrivate
{
    public:
        QxImageManager    *images;
        QxImageManager    *video;
        QxFaceRecognizer  *recognizer;
        QxDataBase        *database;
        QxFeatureDetector *featureDetector;
#ifndef NO_VIDEO
        MainWindow *wind;
#endif

    public:
        int selectedImageLeft;
        int selectedImageRight;

    public:
        bool showInGray;
        bool showImageKeypoints;
        bool showCommonKeypoints;
        bool isClusterColoring;
        bool showBothImages;
        bool isFilterKeypoint;
        bool isShowIds;
        bool isShowHomography;
    public:
        bool  isLeftButton;
        float responseThreshold;
    public:
        QwFaceRecognizerPrivate()
        {
            images = 0;
            video  = 0;
            recognizer = 0;
            database = 0;
            featureDetector = 0;
#ifndef NO_VIDEO
            wind = 0;
#endif


            selectedImageLeft  = 0;
            selectedImageRight = 0;

            showInGray = false;
            showImageKeypoints = true;
            showCommonKeypoints = true;
            isClusterColoring = false;
            showBothImages = true;
            isFilterKeypoint = false;
            isShowIds = true;
            isShowHomography = true;

            isLeftButton      = true;
            responseThreshold = 1000.;
        }

        ~QwFaceRecognizerPrivate()
        {
            images = 0;
            video  = 0;
            recognizer = 0;
            database = 0;
            featureDetector = 0;
        }
};

QwFaceRecognizer::QwFaceRecognizer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QwFaceRecognizer)
{
    ui->setupUi(this);
    data = new QwFaceRecognizerPrivate;

    ui->lblImageLeft->setAutoFillBackground(false);
    ui->lblImageLeft->setAttribute(Qt::WA_PaintOutsidePaintEvent);
    ui->lblImageLeft->installEventFilter(this);

    /*ui->lblImageRight->setAutoFillBackground(false);
    ui->lblImageRight->setAttribute(Qt::WA_PaintOutsidePaintEvent);
    ui->lblImageRight->installEventFilter(this);
*/
    this->setMouseTracking(true);
    ui->listWidget->installEventFilter(this);
    ui->listWidget->viewport()->installEventFilter(this);
  //  ui->lblImageRight->setVisible(data->showBothImages);

    ui->tabWidget->setHidden(true);
    this->on_pushButton_2_clicked();
}

QwFaceRecognizer::~QwFaceRecognizer()
{
    delete data;
    data = 0;
    delete ui;
}

void QwFaceRecognizer::on_actionExit_triggered()
{
    QCoreApplication::exit();
}


void QwFaceRecognizer::setutUI()
{

}

void QwFaceRecognizer::repaint()
{
    if( data )
    {
        ui->lblImageLeft->repaint();
    }
}

void QwFaceRecognizer::drawImage()
{
    if( ui )
    {

#ifndef NO_VIDEO
            if( data->wind != 0 )
            {
                QPainter painter(ui->lblImageLeft);
                painter.fillRect(ui->lblImageLeft->rect(), QColor( 100, 100, 100));
                this->drawFacesRect(painter, ui->lblImageLeft->rect(), data->wind->getFrame());
            }
#endif

        /*if( data != 0)
        {
            if( data->images != 0)
            {
                QRect    rect  = ui->lblImageLeft->rect();
                if( (rect.width() > 10)&&(rect.height()>10))
                {
                    QPainter painter(ui->lblImageLeft);
                    painter.fillRect(rect, QColor( 100, 100, 100));

                    QxImageFrame *leftFrame  = 0;
                    QxImageFrame *rightFrame = 0;

                    if( !data->showBothImages )
                    {
                        if( (data->selectedImageLeft>=0) && (data->selectedImageLeft<data->images->count()))
                        {
                            leftFrame = data->images->getImage(data->selectedImageLeft);
                            if( leftFrame->isValid())
                            {
                                this->drawOne(painter, rect, leftFrame);
                            }
                        }
                    }
                    else
                    {
                        if( (data->selectedImageLeft>=0) && (data->selectedImageLeft<data->images->count()))
                        {
                            leftFrame = data->images->getImage(data->selectedImageLeft);
                            if( leftFrame->isValid())
                            {
#ifndef NO_VIDEO
                                if( data->wind != 0 )
                                {
                                    if( data->wind->getIsDraw() )
                                    {
                                        rightFrame = data->wind->getFrame();
                                        if( rightFrame->isValid())
                                        {
                                            //this->drawTwo(painter, rect, leftFrame, rightFrame);
                                            this->drawFacesRect(painter, rect, leftFrame, rightFrame);
                                        }
                                    }
                                    else
                                    {
                                        if( (data->selectedImageRight>=0) && (data->selectedImageRight<data->images->count()))
                                        {
                                            rightFrame = data->images->getImage(data->selectedImageRight);
                                            if( rightFrame->isValid())
                                            {
                                                //this->drawTwo(painter, rect, leftFrame, rightFrame);
                                                this->drawFacesRect(painter, rect, leftFrame, rightFrame);
                                            }
                                        }
                                    }
                                }
                                else
#endif
                                {
                                    if( (data->selectedImageRight>=0) && (data->selectedImageRight<data->images->count()))
                                    {
                                        rightFrame = data->images->getImage(data->selectedImageRight);
                                        if( rightFrame->isValid())
                                        {
                                            //this->drawTwo(painter, rect, leftFrame, rightFrame);
                                            this->drawFacesRect(painter, rect, leftFrame, rightFrame);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }*/
    }
}

void QwFaceRecognizer::drawFacesRect(QPainter &device, QRect deviceRect, QxImageFrame *frameLeft, QxImageFrame *frameRight)
{
    if( (frameLeft) && (data) )
    {
    if( frameLeft->isValid() )
    {
        /*--------------------------------------*/

        QImage   canvasLeft= QImage(frameLeft->width(),  frameLeft->height(),  QImage::Format_RGB888);

        if( !data->showInGray )
        {
            frameLeft->putImageRGBInto(canvasLeft.bits());
        }
        else
        {
            frameLeft->putImageGrayscaleInto(canvasLeft.bits(), canvasLeft.bytesPerLine());
        }

        QSize canvasSize( deviceRect.size() );
              canvasSize.setWidth( canvasSize.width());

        QSize canvasLeftSize( canvasLeft.size() );
              canvasLeftSize.scale(canvasSize, Qt::KeepAspectRatio);

        QRect canvasLeftRect;
              canvasLeftRect.setSize(canvasLeftSize);
              canvasLeftRect.moveCenter(deviceRect.center());

        device.save();
        device.setWindow  (canvasLeft.rect());
        device.setViewport( canvasLeftRect );

        int r, g, b;

        device.drawImage(canvasLeft.rect(),  canvasLeft);



        if( data->database )
        {
            if( frameLeft->getProb() > 0  )
            {
                QRect rect = frameLeft->getFaceRect();
                rect.setWidth(50);
                rect.setHeight(15);

                if( frameLeft->getProb() >= data->database->getMinLevel() )
                {
                    r = 255 - 200*(frameLeft->getProb());
                    g = 255 - 200*(frameLeft->getProb());

                    if( r< 0 )
                    {
                        r = 0;
                    }

                    if( g < 0 )
                    {
                        g = 0;
                    }
                    device.setPen(QColor(r, 255, g));
                }
                else
                {
                    device.setPen(QColor(255, 0, 0));
                }
                device.drawRect(frameLeft->getFaceRect());

                device.drawText(rect, QString().setNum(frameLeft->getProb()));
            }
        }

        device.restore();

        if( data->wind )
        {
            device.setPen(QColor(255, 0, 255));
            device.drawText(QRect(0, 0, 50, 50), QString().setNum(data->wind->getFps()));
        }
        /*--------------------------------------*/
    }
    }
}

void QwFaceRecognizer::drawFeatureList(QPainter &device, QList<QxImageFeature *> &features, QColor color, bool coloring)
{
    if( data )
    {
        bool isDraw     = true;
        bool isDrawLine = true;

        int   baseSize  = 4;
        float baseScale = 1./16.;

        QxImageFeature *feature;

        float response;
        float angle   ;
        float size    ;
        int sizeInPixel;
        int hash;
        QPoint center;

        QPen circlePen(color);

        unsigned char r = color.red();
        unsigned char g = color.green();
        unsigned char b = color.blue();

        QPen linePen( QColor(255 - r, 255-g, 255 - b, color.alpha()));
        QPen textPen( QColor(255 - r, 255-g, 255 - b, color.alpha()));
        QRect textRect;
        QFont textFont;

        textFont = device.font();
        textFont.setStyleStrategy(QFont::NoAntialias);
        textFont.setStyleHint(QFont::Monospace);

        circlePen.setWidth(2);
        circlePen.setCosmetic(true);

        linePen.setWidth(2);
        linePen.setCosmetic(true);

        textPen.setWidth(10);
        textPen.setCosmetic(true);

        int count = features.count();


        for( int i = 0; i<count; ++i)
        {
            feature = features.at(i);
            if( feature )
            {
                if( feature->isValid() )
                {

                    response = feature->getResponse();
                    angle    = feature->getAngle();
                    size     = feature->getSize();

                    center.setX(feature->x());
                    center.setY(feature->y());

                    sizeInPixel = int(size*baseScale) +  baseSize;

                    if( data->isFilterKeypoint )
                    {
                        if( response<data->responseThreshold )
                        {
                            continue;
                        }
                    }

                    if( coloring )
                    {
                        r = ( i*150+30       )    % 200+50;
                        g = ( (i << 1)*60+70 )    % 200+50;
                        b = ( i*i+90         )    % 200+50;
                    }
                    else
                    {
                        hash = feature->getDescriptorHash();

                        r = color.red();
                        g = color.green();
                        b = color.blue();

                        if( r == 0)
                        {
                            r = ( hash ) % 200+50;
                        }

                        if( g == 0)
                        {
                            g = ( hash ) % 200+50;
                        }

                        if( b == 0)
                        {
                            b = ( hash) % 200+50;
                        }
                    }


                    if( isDraw )
                    {
                        circlePen.setColor( QColor(r, g, b, color.alpha()));

                        device.setPen(circlePen);
                        device.drawEllipse(center, sizeInPixel, sizeInPixel);
                    }

                    if( (coloring) && (data->isShowIds))
                    {
                        textRect.setWidth(2*sizeInPixel);
                        textRect.setHeight(sizeInPixel);

                        textRect.moveCenter(center);

                        textFont.setPointSizeF(sizeInPixel);

                        textPen.setColor( QColor(255-r, 255-g, 255-b, color.alpha()));
                        device.setPen(textPen);
                        device.setFont(textFont);

                        device.drawText(textRect, Qt::AlignCenter, QString().setNum(sizeInPixel));
                    }
                    else
                    {
                        if( isDrawLine )
                        {
                            linePen.setColor( QColor(255-r, 255 - g, 255 - b, color.alpha()));
                            device.setPen(linePen);
                            device.drawLine(center.x(), center.y(), center.x()+sizeInPixel*cos(angle), center.y()+sizeInPixel*sin(angle));
                        }
                    }

                }
            }
        }
    }
}

void QwFaceRecognizer::drawTwo(QPainter &device, QRect deviceRect, QxImageFrame *frameLeft, QxImageFrame *frameRight )
{
    if( (frameLeft) && (data) )
    {
    if( frameLeft->isValid())
    {
        /*--------------------------------------*/
//
        QxImageFeatureList *featuresLeft = 0;
        QxImageFeatureList *featuresRight = 0;

        QList<QxImageFeature *>commonLeft;
        QList<QxImageFeature *>commonRight;
        QList<QxImageFeature *>dismatchesLeft;
        QList<QxImageFeature *>dismatchesRight;

//

        QImage   canvasLeft  = QImage(frameLeft->width(),  frameLeft->height(),  QImage::Format_RGB888);
        QImage   canvasRight = QImage(frameRight->width(), frameRight->height(), QImage::Format_RGB888);

        if( !data->showInGray )
        {
            frameLeft->putImageRGBInto(canvasLeft.bits());
            frameRight->putImageRGBInto(canvasRight.bits());
        }
        else
        {
            frameLeft->putImageGrayscaleInto(canvasLeft.bits());
            frameRight->putImageGrayscaleInto(canvasRight.bits());
        }

        featuresLeft  = frameLeft->getImageFeatureList();
        featuresRight = frameRight->getImageFeatureList();

        if( (featuresLeft) && (featuresRight) )
        {
            featuresLeft->findCommon(featuresRight, &commonLeft, &dismatchesLeft, &commonRight, &dismatchesRight);
        }
        else
        {
            if( featuresRight )
            {
                featuresRight->matchWithFeatures(featuresLeft, &commonRight, &dismatchesRight );
            }
            if( featuresLeft )
            {
                featuresLeft->matchWithFeatures(featuresRight, &commonLeft, &dismatchesLeft );
            }
        }

        QSize canvasSize( deviceRect.size() );
              canvasSize.setWidth( canvasSize.width()/2-5);

        QSize canvasLeftSize( canvasLeft.size() );
              canvasLeftSize.scale(canvasSize, Qt::KeepAspectRatio);

        QSize canvasRightSize( canvasRight.size() );
              canvasRightSize.scale(canvasSize, Qt::KeepAspectRatio);

        QPoint centerPoint( canvasSize.width()/2, canvasSize.height()/2);
        QRect canvasLeftRect;
              canvasLeftRect.setSize(canvasLeftSize);
              canvasLeftRect.moveCenter(centerPoint);

              centerPoint.setX( deviceRect.width()-canvasSize.width()/2);
        QRect canvasRightRect;
              canvasRightRect.setSize(canvasRightSize);
              canvasRightRect.moveCenter( centerPoint );

        QTransform homo;

        device.save();
        device.setWindow  (canvasRight.rect());
        device.setViewport( canvasRightRect );

        /*if( data->isShowHomography )
            if( QxImageFeatureList::findHomography(&commonRight, &commonLeft, homo) )
            {
                device.setWindow  (canvasLeft.rect());
                device.setWorldTransform(homo);
            }*/

        device.drawImage(canvasRight.rect(),  canvasRight);

        if( data->showImageKeypoints )
        {
            this->drawFeatureList(device, dismatchesRight, QColor(0, 0, 255));
        }

        if( data->showCommonKeypoints )
        {
            this->drawFeatureList(device, commonRight, QColor(0, 255, 0), true);
        }

        device.restore();

        device.save();
        device.setViewport( canvasLeftRect );
        device.setWindow( canvasLeft.rect() );

        device.drawImage( canvasLeft.rect(),  canvasLeft);

        if( data->showImageKeypoints )
        {
            this->drawFeatureList(device, dismatchesLeft, QColor(0, 0, 255));
        }

        if( data->showCommonKeypoints )
        {
            this->drawFeatureList(device, commonLeft, QColor(0, 255, 0), true);
        }
        device.restore();

        /*--------------------------------------*/
    }
    }
}


void QwFaceRecognizer::drawOne(QPainter &device, QRect deviceRect, QxImageFrame *frame)
{
    if( (frame) && (data) )
    {
    if( frame->isValid())
    {
        /*--------------------------------------*/

        QImage   canvas = QImage(frame->width(), frame->height(), QImage::Format_RGB888);;
        QPainter painter( &canvas );
        QSize    canvasSize( canvas.size() );
                 canvasSize.scale(deviceRect.size(), Qt::KeepAspectRatio);
        QRect    canvasRect;
                 canvasRect.setSize(canvasSize);
                 canvasRect.moveCenter(deviceRect.center());


        QPen circlePen(QColor(255, 0, 0));
             circlePen.setWidth(2);
        QPen linePen( QColor(100, 100, 220));
             linePen.setWidth(2);

        unsigned char red   = 255;
        unsigned char green = 255;
        unsigned char blue  = 255;

        float baseScale = 1./16.;
        int   baseSize = 4;
        int   sizeInPixel;

//
        QPoint center;
        float size;
        float response;
        float angle;
        int   index;
//
        bool isInBase = false;
        bool isDraw;
        bool isDrawLine;

        QxImageFeatureList *features = 0;
        QxImageFeature     *feature  = 0;
        int featureCount;
//

        if( !data->showInGray )
        {
            frame->putImageRGBInto(canvas.bits());
        }
        else
        {
            frame->putImageGrayscaleInto(canvas.bits());
        }

        features = frame->getImageFeatureList();
        if( !features )
            return;

        featureCount = features->count();

        for( int i=0; i<featureCount; ++i)
        {
            isDraw     = false;
            isDrawLine = false;
            feature = features->getFeature(i);

            red   = 255;
            green = 0;
            blue  = 0;

            if( feature )
            {
                if( feature->isValid() )
                {
                    response = feature->getResponse();
                    angle    = feature->getAngle();
                    size     = feature->getSize();

                    center.setX(feature->x());
                    center.setY(feature->y());

                    sizeInPixel = int(size*baseScale) +  baseSize;

                    if( data->showCommonKeypoints )
                        isDraw = data->database->checkFeature(feature, &index);

                    if( isDraw )
                    {
                        isDrawLine = true;
                        red   = 0;
                        green = ( (index+1)*70) % 200+50;
                        blue  = 0;
                    }
                    else
                    {
                        if( data->showImageKeypoints )
                        {
                            isDrawLine = true;
                            isDraw = true;

                            isDrawLine = true;

                            red   = 0;
                            green = 0;
                            blue  = ( (index+1)*70) % 200+50;
                        }
                    }
                }
                else
                {
                    if( feature->isPoint() )
                    {
                        sizeInPixel = baseSize;
                        center.setX(feature->x());
                        center.setY(feature->y());
                        isDraw = true;
                    }
                }
            }

            if( isDraw )
            {
                circlePen.setColor( QColor(red, green, blue ));
                painter.setPen(circlePen);
                painter.drawEllipse(center, sizeInPixel, sizeInPixel);
            }
            if( isDrawLine )
            {
                painter.setPen(linePen);
                painter.drawLine(center.x(), center.y(), center.x()+sizeInPixel*cos(angle), center.y()+sizeInPixel*sin(angle));
            }
        }

        device.drawImage( canvasRect, canvas);

        /*--------------------------------------*/
    }
    }
}

void QwFaceRecognizer::drawPreviews()
{

}

QPixmap *QwFaceRecognizer::createIcon(QxImageFrame *frame)
{
    return createIcon(frame, 128, 128);
}

QPixmap *QwFaceRecognizer::createIcon(QxImageFrame *frame, QSize &size)
{
    QPixmap *result = 0;
    if( (frame) && (size.isValid()) )
    {
        if( frame->isValid())
        {
            QImage image(frame->getImageRGBBits(), frame->width(), frame->height(), QImage::Format_RGB888);
            QRect sizeRect;
            QRect iconRect;

            QSize icoSize(frame->width(), frame->height());
            icoSize.scale(size, Qt::KeepAspectRatio);

            sizeRect.setSize(size);
            iconRect.setSize(icoSize);

            QPixmap  *icon = new QPixmap(size);
            QPainter iconPainter(icon);

            iconPainter.fillRect(sizeRect, QColor(0,0,0,255));
            iconRect.moveCenter(sizeRect.center());
            iconPainter.drawImage(iconRect, image);
            result = icon;
        }
    }
    return result;
}

QPixmap *QwFaceRecognizer::createIcon(QxImageFrame *frame, int width, int height)
{
    QSize size(width, height);
    return createIcon(frame, size);
}

/*---------------------------------------------------------------*/
bool QwFaceRecognizer::eventFilter(QObject *obj, QEvent *ev)
{
    bool result = false;
    if( obj == ui->lblImageLeft )
    {
        switch( ev->type())
        {
            case QEvent::Paint:
                this->drawImage();
                result = true;
            break;
        }
    }

    if( obj == ui->listWidget->viewport() )
    {
        switch( ev->type())
        {
            case QEvent::MouseButtonPress:
                QMouseEvent *evt = (QMouseEvent *)(ev);
                if( evt->button() == Qt::RightButton )
                {
                    data->isLeftButton = false;
                }
                else
                {
                    data->isLeftButton = true;
                }
                result = false;
            break;
        }
    }

    return result;
}

//************************************************************
//
//  Setters functions start
//
//************************************************************

void QwFaceRecognizer::setImages    ( QxImageManager   *images)
{
    if( data != 0)
    {
        data->images = images;
    }
}

void QwFaceRecognizer::setRecognizer( QxFaceRecognizer *recognizer)
{
    if( data != 0)
    {
        data->recognizer = recognizer;
    }
}

void QwFaceRecognizer::setVideo     ( QxImageManager   *video)
{
    if( data != 0)
    {
        data->video = video;
    }
}

void QwFaceRecognizer::setDatabase  ( QxDataBase       *database)
{
    if( data != 0)
    {
        data->database = database;
    }
}

void QwFaceRecognizer::setFeatureDetector( QxFeatureDetector *detector)
{
    if( data != 0)
    {
        data->featureDetector = detector;
    }
}

#ifndef NO_VIDEO
void QwFaceRecognizer::setMainWindow( MainWindow *wnd)
{
    if( data != 0)
    {
        data->wind = wnd;

        if( data->wind )
        {
            data->wind->setDrawWidget( ui->lblImageLeft );
        }
    }
}
#endif
//############################################################
//  System functions end
//############################################################


//************************************************************
//
//  Setters functions start
//
//************************************************************

void QwFaceRecognizer::loadImages()
{
    QString path;
#ifdef HOME_VERSION
    #ifdef OS_WINDOWS
        path = QString("D:/Head data/");
    #endif

    #ifdef OS_MAC_OS
        path = QString("/Users/Rod/Project/Images/");
    #endif
#endif

#ifdef RELEASE_VERSION
    #ifdef OS_WINDOWS
                path = QDir::currentPath()+QString("/Data/");
    #endif

    #ifdef OS_MAC_OS
                Error!!!
    #endif
#endif


//path = QDir::currentPath()+QString("/Data/");
               // qDebug()<<path;
    if( data != 0 )
    {
        if( data->images != 0)
        {
            data->selectedImageLeft = 0;
            data->selectedImageRight = 0;

            ui->listWidget->clear();
            data->images->clear();
            data->database->clear();

            QStringList files;
            QDirIterator dir( path, QDir::Files | QDir::Readable);
            QString file;
            QImage image;
            QxImageFrame *originFrame = 0;
            QxImageFrame *frame = 0;
            QListWidgetItem *item = 0;
            QPixmap ico(128, 128);
            QPainter painter(&ico);
            QxRescaleFilter rescaler(0.5, true);

            while(dir.hasNext())
            {
                dir.next();
                file = dir.fileInfo().completeSuffix().toLower();
                if( file== "jpg" || file == "png" || file == "jpeg")
                {
                    files<<dir.fileName();
                }
            }

            files.sort();
            QList<QxImageFrame *> frames;
            for( int i=0; i<files.count(); i++)
            {
                file = files.at(i);

                if( image.load(path+file) )
                {
                    if( image.format() != QImage::Format_RGB888 )
                    {
                        image = image.convertToFormat(QImage::Format_RGB888);
                    }
                    originFrame = new QxImageFrame;

                    std::cout<<"bytes: "<<image.byteCount()<<" size: "<<image.bytesPerLine()*image.height()<<" depth: "<<image.depth()<<std::endl;
                    if( !originFrame->imageFromData(image.width(), image.height(), image.bits(), image.bytesPerLine()) )
                    {
                        delete originFrame;
                        originFrame = 0;
                    }
                    else
                    {
                        data->images->addImage(originFrame);
                        frames.push_back(originFrame);
                        //data->database->proceedImageAndAdd(originFrame);
                        item = new QListWidgetItem;
                        item->setIcon(QIcon(*createIcon(originFrame)));
                        item->setText(file+" ("+QString().setNum(originFrame->width())+"x"+QString().setNum(originFrame->height())+")");
                        ui->listWidget->addItem(item);

                        /*rescaler.setIsSaveResolution(true);
                        rescaler.setScaleFactor(0.25);
                        frame = rescaler.applyAndCopy(originFrame);
                        delete originFrame;

                        rescaler.setScaleFactor(1);
                        originFrame = rescaler.applyAndCopy(frame);

                        rescaler.setIsSaveResolution(false);*/
                        /*for( int z = 0; z< 1; z++)
                        {

                            frame = originFrame;//rescaler.applyAndCopy(originFrame);
                            rescaler.setScaleFactor(0.5*rescaler.getScaleFactor());
                            if( frame )
                            {
                                data->images->addImage(frame);

                                if( i<3 )
                                {
                                    data->database->proceedImageAndAdd(frame);
                                }
                                else
                                {
                                    data->database->proceedImage(frame);
                                }

                                item = new QListWidgetItem;
                                item->setIcon(QIcon(*createIcon(frame)));
                                item->setText(file+" ("+QString().setNum(frame->width())+"x"+QString().setNum(frame->height())+")");
                                ui->listWidget->addItem(item);
                                frame = 0;
                            }
                            if( i>=3 )
                                break;
                        }*/
                    }
                }
            }
            //data->database->proceedCandidates();
            data->database->update(frames);
            ui->lblImageLeft->repaint();
        }
    }
}

void QwFaceRecognizer::loadDatabase()
{

}

void QwFaceRecognizer::addImageData()
{

}

void QwFaceRecognizer::rebuildDatabase()
{

}

void QwFaceRecognizer::applyClicked()
{
    this->on_pushButton_2_clicked();
}

//############################################################
//  Proceed functions end
//############################################################

void QwFaceRecognizer::on_actionLoad_images_triggered()
{
    this->loadImages();
}

void QwFaceRecognizer::on_actionRBG_mode_triggered()
{
    data->showInGray = false;
    ui->lblImageLeft->repaint();
}

void QwFaceRecognizer::on_actionGrayscale_mode_triggered()
{
    data->showInGray = true;
    ui->lblImageLeft->repaint();
}

void QwFaceRecognizer::on_listWidget_activated(const QModelIndex &index)
{
    if( data != 0)
    {
        if( data->images != 0)
        {
            if ((index.column() >=0) && ( index.column()<data->images->count() ))
            {
                if( data->isLeftButton )
                {
                    data->selectedImageLeft = index.column();
                }
                else
                {
                    data->selectedImageRight = index.column();
                }
                ui->lblImageLeft->repaint();
            }
        }
    }
}

void QwFaceRecognizer::on_listWidget_currentRowChanged(int currentRow)
{
    if( data != 0)
    {
        if( data->images != 0)
        {
            if ((currentRow >=0) && ( currentRow<data->images->count() ))
            {
                if( data->isLeftButton )
                {
                    data->selectedImageLeft = currentRow;
                }
                else
                {
                    data->selectedImageRight = currentRow;
                }
                QxImageFrame *frame = data->images->getImage(currentRow);
                QxImageFeatureList *lst = frame->getImageFeatureList();
                QxImageFeature *feature;
                for( int i = 0; i<lst->count(); ++i)
                {
                    feature = lst->getFeature(i);
                }
                ui->lblImageLeft->repaint();
            }
        }
    }
}

void QwFaceRecognizer::on_checkBox_clicked(bool checked)
{
    if( data )
    {
        data->showCommonKeypoints = checked;
        ui->lblImageLeft->repaint();
    }
}

void QwFaceRecognizer::on_checkBox_2_clicked(bool checked)
{
    if( data )
    {
        data->showImageKeypoints = checked;
        ui->lblImageLeft->repaint();
    }
}

void QwFaceRecognizer::on_chkShowRGB_clicked(bool checked)
{
    if( data )
    {
        data->showInGray = !checked;
        ui->lblImageLeft->repaint();
    }
}

void QwFaceRecognizer::on_pushButton_2_clicked()
{
    if( data )
    {
        if( data->featureDetector )
        {
            switch( ui->cmbDetectors->currentIndex() )
            {
                default:
                case 0:
                    data->featureDetector->setDetectorType(fdSURF64);
                break;

                case 1:
                    data->featureDetector->setDetectorType(fdSURF128);
                break;

                case 2:
                    data->featureDetector->setDetectorType(fdSIFT);
                break;
            }
            data->featureDetector->setHessianThreshold(ui->surfHessian->value());
            data->featureDetector->setOctaveLayers(ui->surfOcataveLayers->value());
            data->featureDetector->setOctaves(ui->surfOctaves->value());
        }
    }
}

void QwFaceRecognizer::on_pushButton_clicked()
{
    if( data )
    {
        if( (data->images) && (data->database))
        {
            ui->lblImageLeft->setVisible(false);
            QxImageFrame *frame;
            data->database->clear();

            for(int i=0; i<data->images->count(); ++i )
            {
                frame = data->images->getImage(i);
                data->database->proceedImageAndAdd(frame);
            }
            data->database->proceedCandidates();
            ui->lblImageLeft->setVisible(true);
        }
    }
}

void QwFaceRecognizer::on_chkShowRight_clicked(bool checked)
{
    if ( data )
    {
        data->showBothImages = checked;

        ui->lblImageLeft->repaint();
        //ui->lblImageRight->repaint();
        //ui->lblImageRight->setVisible(data->showBothImages);
    }
}

void QwFaceRecognizer::on_listWidget_clicked(const QModelIndex &index)
{

}

void QwFaceRecognizer::on_doubleSpinBox_editingFinished()
{
    if( data )
    {
        if( data->database )
        {
            data->database->setClustering(ui->doubleSpinBox->value());
        }
    }
}

void QwFaceRecognizer::on_doubleSpinBox_2_editingFinished()
{
    if( data )
    {
        if( data->database )
        {
            data->database->setTreshhold(ui->doubleSpinBox_2->value());
        }
    }
}

void QwFaceRecognizer::on_checkBox_6_clicked(bool checked)
{
    if( data )
    {
        data->isFilterKeypoint = checked;
        this->repaint();
    }
}

void QwFaceRecognizer::on_doubleSpinBox_3_editingFinished()
{
    if( data )
    {
        data->responseThreshold = ui->doubleSpinBox_3->value();
        this->repaint();
    }
}

void QwFaceRecognizer::on_actionLoad_database_triggered()
{
    if( data )
        if( data->database )
        {
            QString fileName = QFileDialog::getOpenFileName(this, "Load model",QString("*.mdl"));
            if(!fileName.isNull())
            {
               data->database->load(fileName);
            }
        }
}

void QwFaceRecognizer::on_actionSave_database_triggered()
{
    if( data )
        if( data->database )
        {
            QString fileName = QFileDialog::getSaveFileName(this, "Save model",QString("*.mdl"));
            if(!fileName.isNull())
            {
               data->database->save(fileName);
            }
        }
}

#ifndef QWFACERECOGNIZER_H
#define QWFACERECOGNIZER_H

#include <QMainWindow>
#include <QDir>
#include <QFile>
#include <QDirIterator>
#include <QPainter>
#include <QImage>
#include <QListWidgetItem>
#include <QPixmap>
#include <QMouseEvent>
#include <QFileDialog>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/video/background_segm.hpp>

#include "../Global.h"

#include "../QxImageProcessor/QxImageClasses.h"
#include "../QxImageProcessor/QxDataBase.h"
#include "../QxImageProcessor/QxFaceRecognizer.h"
#include "../QxImageProcessor/QxImageFrame.h"
#include "../QxImageProcessor/QxImageManager.h"
#include "../QxImageProcessor/QxFeatureDetector.h"
#include "../QxImageProcessor/QxImageFilter.h"

#ifndef NO_VIDEO
    #include "../mainwindow.h"
#endif

namespace Ui {
class QwFaceRecognizer;
}

class QwFaceRecognizerPrivate;
class QwFaceRecognizer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit QwFaceRecognizer(QWidget *parent = 0);
    virtual ~QwFaceRecognizer();
public:
    void setutUI();
    void repaint();
    void drawImage();
    void drawOne(QPainter &device, QRect deviceRect, QxImageFrame *frame);
    void drawTwo(QPainter &device, QRect deviceRect, QxImageFrame *frameLeft, QxImageFrame *frameRight );

    void drawFacesRect(QPainter &device, QRect deviceRect, QxImageFrame *frameLeft, QxImageFrame *frameRight = 0);
    void drawFeatureList(QPainter &device, QList<QxImageFeature *> &features, QColor color, bool coloring = false);

    void drawPreviews();
    QPixmap *createIcon(QxImageFrame *frame);
    QPixmap *createIcon(QxImageFrame *frame, QSize &size);
    QPixmap *createIcon(QxImageFrame *frame, int width, int height);
    virtual bool eventFilter(QObject *obj, QEvent *ev);
public:
    void setImages    ( QxImageManager   *images);
    void setRecognizer( QxFaceRecognizer *recognizer);
    void setVideo     ( QxImageManager   *video);
    void setDatabase  ( QxDataBase       *database);
    void setFeatureDetector( QxFeatureDetector *detector);
#ifndef NO_VIDEO
    void setMainWindow( MainWindow *wnd);
#endif
public:
    void loadImages();
    void loadDatabase();
public:
    void addImageData();
    void rebuildDatabase();

    void applyClicked();
private slots:
    void on_actionExit_triggered();

    void on_actionLoad_images_triggered();

    void on_actionRBG_mode_triggered();

    void on_actionGrayscale_mode_triggered();

    void on_listWidget_activated(const QModelIndex &index);

    void on_listWidget_currentRowChanged(int currentRow);

    void on_checkBox_clicked(bool checked);

    void on_checkBox_2_clicked(bool checked);

    void on_chkShowRGB_clicked(bool checked);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_chkShowRight_clicked(bool checked);

    void on_listWidget_clicked(const QModelIndex &index);

    void on_doubleSpinBox_editingFinished();

    void on_doubleSpinBox_2_editingFinished();

    void on_checkBox_6_clicked(bool checked);

    void on_doubleSpinBox_3_editingFinished();

    void on_actionLoad_database_triggered();

    void on_actionSave_database_triggered();

private:
    Ui::QwFaceRecognizer *ui;
    QwFaceRecognizerPrivate *data;

};

#endif // QWFACERECOGNIZER_H
